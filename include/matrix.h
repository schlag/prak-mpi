#ifndef MATRIX_H_
#define MATRIX_H_
/* (c) 1997 Peter Sanders */
/* only this is platform specific and only works on IBM machines */

#ifndef  _CMPLX
#define  _CMPLX 1
#ifndef  _REIM
#define _REIM   1
#endif
typedef union { struct { float  __re, __im; } __data; double __align1; }  cmplx;
#endif
 
#ifndef  _DCMPLX
#define  _DCMPLX 1
#ifndef  _REIM
#define _REIM   1
#endif
typedef union { struct { double __re, __im; } __data; double __align1; } dcmplx;
#endif

#include <cblas.h>

#define matMatSquare(n, alpha, beta, A, B, C)\
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, (n), (n), (n), (alpha), (A), (n), (B), (n), \
         (beta), (C), (n))  

/* C = AB */
#define matMatMult(n, A, B, C) matMatSquare(n, 1.0, 0.0, A, B, C)

/* C += AB */
#define matMatMultAdd(n, A, B, C) matMatSquare(n, 1.0, 1.0, A, B, C)

/* compute y = A * x where A is a n x n square matrix
 * prototype would be
 * void matVecMult(int n, double *A, double *x, double *y);
 *
 * implemented by calling a function computing y = 0.0 * y + 1.0 * A * x
 */
#define matVecMult(n, A, x, y)\
    cblas_dgemv(CblasRowMajor, CblasNoTrans, (n), (n), 1.0, (A), (n), (x), 1, 0.0, (y), 1) 

#endif /* MATRIX_H_ */
