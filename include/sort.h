#ifndef SORT
#define SORT
/* (c) 1997 Peter Sanders */
#include <stdlib.h>


#ifndef  _CMPLX
#define  _CMPLX 1
#ifndef  _REIM
#define _REIM   1
#endif
typedef union { struct { float  __re, __im; } __data; double __align1; }  cmplx;
#endif
 
#ifndef  _DCMPLX
#define  _DCMPLX 1
#ifndef  _REIM
#define _REIM   1
#endif
typedef union { struct { double __re, __im; } __data; double __align1; } dcmplx;
#endif


/* the following is not available in HC3 */
/* #include <essl.h> */

/* Fast integer sort from essl.h
 * This routine and essl.h is machine dependent
 */
/* #define fastIntSort(item, size) isort(item, 1, size) */


/* qsort from stdlib: */
#define intSort(item, size) qsort(item, size, sizeof(int), intCmp)

/* initializes key[0]..key[n-1] with random keys 
 * also works in parallel
 */
void randInitBound(int *key, int n, int bound);

#define randInit(key, n) randInitBound(key, n, (500000000));

/* are your items sorted and not larger than those of you neighbor? */
int isGloballySorted(int *item, int size);


/* gather all items at PE 0 and print them */
void printItemsGlobally(int *item, int size);


/* comparison function for quicksort */
int intCmp(const void *a, const void *b);


/* rearrange item[0]..item[size-1] according to pivot.
 * return an index i such that 
 * for all i <= j < size: item[j] >= pivot and
 * for all 0 <= j < i   : item[j] <= pivot 
 * the algorithm used is similar to the Quicksort partition 
 * algorithm described in "Duden Informatik"
 */
int split(int *item, int size, int pivot);


/* partition item[0]..item[size-1] into k parts 
 * according to pivot[1]..pivot[k-1] such that
 * forall 0 <= i < k 
 *    forall start[i] <= x < start[i+i]
 *       pivot[i] <= *x <= pivot[i+1]
 * (assume pivot[0] = -infinity, pivot[k] = infinity) 
 * pivot[0] and pivot[k] are never really accessed
 */ 
void partition(int *item, int size, int *pivot, int **start, int k);

#endif
