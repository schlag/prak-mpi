#ifndef TIMING
#define TIMING
/* (c) 1996, 1997 Rainer Typke, Peter Sanders */
/* A simple function for reliable timing.
 * Two main advantages:
 * - More robust to noise than averaging
 * - Even works if there are large deviations due
 *   to external reasons
 * Warning: Do not use if large deviations from the
 * average execution time can happen without
 * external interference
 */

/* Input:


   double *time:  array of measurement results
   int n:         number of array elements in time
   double alpha:  number in [0, 0.5) that specifies how many array elements
                  should NOT be considered - the smallest and greatest 
                     floor(alpha * n)
                  elements are dropped.

   Result:

                  Average of the values in the array without the
                     floor(alpha * n)
                  greatest and floor(alpha * n) smallest elements.

*/

/* C++ compatibility */
#ifdef __cplusplus
#define CC extern "C"
#else
#define CC
#endif


CC double pruned_average(double *time, int n, double alpha);
#endif
