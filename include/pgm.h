
#ifndef H_PGM
#define H_PGM

#ifdef __cplusplus
#define CC extern "C"
#else
#define CC
#endif

/* 
 * bild immer quadratisch
 * daten als lineares feld von int
 * werte im feld aus [0..m_max]
 */

CC void pgm ( char * filename , int * data , int size , int m_max ) ;

#endif
