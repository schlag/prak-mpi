#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#ifdef __cplusplus
#define CC extern "C"
#else
#define CC
#endif

extern char *bitmapBuffer;
extern unsigned int bitmapXSize, bitmapYSize;

/* some utitility macros */
#define bitmapSetPixel(x, y)\
  bitmapBuffer[(y) * ((bitmapXSize + 7)/8) + ((x)>>3)] |= (1 << ((x)&7))
#define bitmapClearPixel(x, y)\
  bitmapBuffer[(y) * ((bitmapXSize + 7)/8) + ((x)>>3)] &= ~(1 << ((x) & 7))


CC void closeAll(void);
CC void bitmapInit(int bwidth, int bheight, char *displayName, char *title);
CC void bitOrder(void);
CC void bitmapDisplay(void);
CC void eventLoop(void);

/*
static int reverse_byte(int );
static void createWindow(char *displayName, char *title);
static void redraw(void);
static int process_event(XEvent *event);
*/
