/**
 * Used to map two dimensional array indizes into
 * a single dimension.
 */
#define addr(i,j,n) i*n+j


int assertMatrixEquals(int n, double *reference, double *actual);

int assertMatrixEquals2(int xdim, int ydim, double *reference, double *actual);

void showMatrix(double *mat, int n);

void showMatrix2(double *mat, int xdim, int ydim);

enum matrixkind {ENUM, IDENTITY, ZERO, SPECIAL_A, SPECIAL_B};

void matInitCreateType(double *mat, int n, enum matrixkind kind);

void matInitEnumerate(double *mat, int n);

void matInitZero(double *mat, int n);

void matInitZero2(double *mat, int xdim, int ydim);

void matInitIdentity(double *mat, int n);

/* mat[i][j] = i / (j+1) */
void matInitSpecialA(double *mat, int n);

/* mat[i][j] = (i+1) / (n*(j+1)) */
void matInitSpecialB(double *mat, int n);

/* vec[i] = i+1 */
void vecInitSpecial(double *vec, int n);
