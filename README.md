# Message Passing Lab

A lab which follows the instructions written down in the book 'Parallele Programmierung mit MPI: ein Praktikum'.

The lab is concerned with important aspects of the parallelization of programs and algorithms via message-passing. In
particular, it covers parallel sorting, parallel matrix- and vector-multiplication, parallel simulation
of cellular automatons and loadbalancing of regular and irregular tasks.


## Workflow

 1. Hack away within `src/foo`
 2. Run `./bin/deplay foo` to push these changes to the cluster.
 3. SSH into the cluster using `ssh -X kk03@hc3.scc.kit.edu`.
 4. Run the exeriments on the cluster. Go back to 1 if needed.
 5. Issue `./bin/fetch foo` to pull any generated graphs/reports into `./tmp/`.


## Common Commands

    xccc foo.c -o foo    # compile
    xcinter 4 foo        # run locally on 4 PEs
    xcbatch 64 foo       # start as batch job using 64 PEs
    job_queue            # view scheduled batch jobs
    job_cancel PID       # cancel the scheduled job with the given PID

## Course Information

Email graphs, documentation and sources to the instructor.

To roll a release tarball simply issue `./bin/release DescriptiveName src/foo tex/foo` (e.g.,`./bin/release Zeitmessung src/pingpong tex/03_grundlagen`).
The release file is saved in `./release/` and ready to send.

## Links

* [Lab Page](http://liinwww.ira.uka.de/~thw/prak-mpi/)
