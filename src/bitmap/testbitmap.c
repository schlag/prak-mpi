#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <strings.h>
#include <unistd.h>
#include "bitmap.h"


int main (int argc, char *argv[])
{
   int myId, nrPEs;
   int height, width;
   char display[1024];
   double startTime, waitingTime;
   int i, j;

   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &myId);
   MPI_Comm_size(MPI_COMM_WORLD, &nrPEs);

   if (argc != 3) {
      if (myId==0){
         /* print some helping information */
         printf("Usage:\n");
         printf("%s <windowheight> <display>\n", argv[0]);
         printf("   where <windowheight> must be a multiple of nr of procs.\n");
      }

      MPI_Finalize();
      exit(1);
   }

   height = atoi(argv[1]);
   strcpy (display, argv[2]);
   width = 400;

   bitmapInit(width, height, display, "BITMAP TEST", MPI_COMM_WORLD);

   for (i=0; i<height; i++) {
      for (j=0; j<width; j++) {
         if ( ((i+j)%(15*myId+4)) == 0 ) {
            bitmapSetPixel(j,i);
         }
         else {
             bitmapClearPixel(j,i);
         }

      }
   }

   bitmapDisplay();

   startTime = MPI_Wtime();
   waitingTime = 1*(myId+1);
   printf("Process %d will redraw its part of the window in %3.1f seconds\n", myId, waitingTime);
   while ( (startTime+waitingTime) > MPI_Wtime() ) {}

   for (i=0; i<height; i++) {
      for (j=0; j<width; j++) {
         if ( ((i-j)%(5*myId+5)) != 0 ) {
            bitmapSetPixel(j,i);
         }
         else {
            bitmapClearPixel(j,i);
         }
      }
   }
   bitmapDisplay();

   MPI_Barrier(MPI_COMM_WORLD);
   if (myId==0) {
      puts("Press Enter to exit from program.");
      scanf("%c", &i);
   }
   MPI_Barrier(MPI_COMM_WORLD);

   MPI_Finalize();
   return 1;
}
