#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <strings.h>
#include <unistd.h>
#include "bitmap.h"


int main (int argc, char *argv[])
{
   int myId, nrPEs;
   int totalHeight, myHeight, width;
   char display[1024];
   double startTime, waitingTime;
   int i, j;

   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &myId);
   MPI_Comm_size(MPI_COMM_WORLD, &nrPEs);

   if (argc != 3) {
      if (myId==0){
         /* print some helping information */
         printf("Usage:\n");
         printf("%s <windowheight> <display>\n", argv[0]);
         printf("   where <windowheight> must be a multiple of nr of procs.\n");
      }

      MPI_Finalize();
      exit(1);
   }

   totalHeight = atoi(argv[1]);
   strcpy (display, argv[2]);
   width = 400;

   myHeight = totalHeight/nrPEs;

   bitmapInitBanded(width, totalHeight, display, "Bitmap Test", myHeight, MPI_COMM_WORLD);

   for (i=0; i<myHeight; i++) {
      for (j=0; j<width; j++) {
         if ( ((i+j)%(5*myId+5)) == 0 ) {
            bitmapSetPixel(j,i);
         }
         else {
             bitmapClearPixel(j,i);
         }

      }
   }

   bitmapDisplayBand(myId*myHeight, myHeight);

#if 0
   MPI_Barrier(MPI_COMM_WORLD);

   bitmapDisplayBand(myId*myHeight, myHeight);

   MPI_Barrier(MPI_COMM_WORLD);
#endif

   startTime = MPI_Wtime();
   waitingTime = 1*(myId+1);
   printf("Process %d will redraw its part of the window in %3.1f seconds\n", myId, waitingTime);
   while ( (startTime+waitingTime) > MPI_Wtime() ) {}

   for (i=0; i<myHeight; i++) {
      for (j=0; j<width; j++) {
         if ( ((i-j)%(15*myId+4)) != 0 ) {
            bitmapSetPixel(j,i);
         }
         else {
            bitmapClearPixel(j,i);
         }
      }
   }
   printf("Process %d will redraw its part of the window now.\n", myId);
   bitmapDisplayBand(myId*myHeight, myHeight);

   MPI_Barrier(MPI_COMM_WORLD);
   if (myId==0) {
      puts("Press Enter to exit from program.");
      scanf("%c", &i);
   }
   MPI_Barrier(MPI_COMM_WORLD);

   MPI_Finalize();
   return 1;
}
