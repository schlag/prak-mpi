#include "bitmap.h"
#include <string.h>

int screen=0;
Display *dis=NULL;
Window win=0;

GC gc=0;

XImage *im=NULL;

int reverseBits;
char *bitmapBuffer;
unsigned int bitmapXSize, bitmapYSize;

static int reverse_byte( int x );
static void createWindow(char *displayName, char *title);
static void redraw(void);
static int process_event(XEvent *event);

void closeAll(void)
{
  if(im && bitmapBuffer){
    XDestroyImage(im);
    bitmapBuffer=NULL;
  }
  if(bitmapBuffer)
    free(bitmapBuffer);
  if(win)
    XDestroyWindow(dis, win);
  if(dis)
    XCloseDisplay(dis);
}

void bitmapInit(int bwidth, int bheight, char *displayName, char *title)
{
  Visual *v=NULL;
  
  bitmapXSize=bwidth;
  bitmapYSize=bheight;

  createWindow(displayName, title);
  
  
  if(NULL == (bitmapBuffer = (char *)
	      calloc( 1, ((bitmapXSize+7)/8 )*bitmapYSize))){
    fprintf(stderr,"\nmalloc error\n");
    closeAll();
    exit(1);
  }

  v = DefaultVisual(dis, screen);
  if(NULL == (im = XCreateImage(dis, v, 1, XYBitmap,
				0, bitmapBuffer, bitmapXSize, bitmapYSize,
				8, 0)))
  {
    fprintf(stderr,"\nXCreateImage error\n");
    closeAll();
    exit(1);
  }
}

void bitOrder(void)
{
  if(BitmapBitOrder(dis) == LSBFirst){
    int i;
    i=bitmapYSize * ((bitmapXSize + 7) / 8);
    while(i--){
      bitmapBuffer[i] = reverse_byte( bitmapBuffer[i] );
    }
  }
}

void bitmapDisplay(void)
{
  redraw();
  XFlush(dis);
}

void eventLoop(void)
{
  XEvent event;

  while(1){

    XNextEvent( dis, &event );
    if(process_event(&event))
      break;
  }
}

static int reverse_byte( int x )
{
  unsigned int bit, byte, bytenew = 0;
  byte = x & 0x000000ff;
  for(bit=0; bit < 8; bit++){
    bytenew |= (((byte & (1<<bit)) >> bit) <<(7-bit));
  }
  return bytenew & 0x000000ff;
}

static void createWindow(char *displayName, char *title)
{
  Window rootwin=0;
  Colormap cmap;
  char black[]="#000000";
  char white[]="#ffffff";
  XColor white_col, black_col;

  int depth=0;
  XSizeHints size_hints;
  int border_width = 4;

  if(NULL == (dis=XOpenDisplay(displayName))){
    fprintf(stderr,"\nXOpenDisplay error\n");
    exit(1);
  }

  reverseBits= (BitmapBitOrder(dis)==LSBFirst);

  screen=DefaultScreen(dis);
  depth=DefaultDepth(dis,screen);
  rootwin=RootWindow(dis, screen);

  if( !(win=XCreateSimpleWindow(dis, rootwin,
				0,0,bitmapXSize,bitmapYSize,
				border_width,
				BlackPixel(dis,screen),
				BlackPixel(dis,screen))))
  {
    fprintf(stderr,"\nXCreateWindow error\n");
    closeAll();
    exit(1);
  }

  assert(cmap=DefaultColormap(dis,screen));

  XParseColor(dis, cmap, black, &black_col);
  XParseColor(dis, cmap, white, &white_col);

  assert(XAllocColor(dis, cmap, &black_col));
  assert(XAllocColor(dis, cmap, &white_col));

  assert(gc = XCreateGC(dis, rootwin, 0, 0));
  XSetForeground(dis, gc, black_col.pixel);
  XSetBackground(dis, gc, white_col.pixel);  
  
       
  size_hints.flags = PSize | PMinSize | PMaxSize;
  size_hints.min_width = bitmapXSize;
  size_hints.max_width = bitmapXSize;
  size_hints.min_height = bitmapYSize;
  size_hints.max_height = bitmapYSize;

  XSetStandardProperties(dis, win, title, title, None,
			 0, 0, &size_hints); 
  

  XSelectInput(dis, win, ExposureMask | KeyPressMask );
  
  XMapWindow(dis, win);
  XFlush(dis);
}

static void redraw(void)
{
  XPutImage(dis, win, gc, im, 0, 0, 0, 0, bitmapXSize, bitmapYSize);
}

static int process_event(XEvent *event)
{
  KeySym key;
  switch(event->type){
  case KeyPress:
    key=XLookupKeysym(&event->xkey,0);
    switch(key){
    case XK_q:
    case XK_n:
      return 1;
      break;
    default:
      break;
    }
    return 0;
    break;
  case Expose:
    redraw();
    return 0;
    break;
  default:
    return 0;
    break;
  }
}

/*
int main(int argc, char **argv)
{
  int i,j;

  bitmapInit(80,50,argv[1],"TEST");

  
  for(j=0;j<50;j++){
    for(i=0;i<10;i++){
      if(i>=5 && i<8)
	bitmapBuffer[j*10+i]=0xff;
      else
	bitmapBuffer[j*10+i]=0x00;
    }
  }
    
  eventLoop();

  closeAll();
  return 0;
}
*/
