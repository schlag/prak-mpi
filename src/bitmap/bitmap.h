#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

extern char *bitmapBuffer;
extern unsigned int bitmapXSize, bitmapYSize;

void closeAll(void);
void bitmapInit(int bwidth, int bheight, char *displayName, char *title);
void bitmapDisplay(void);
void eventLoop(void);

/*
static int reverse_byte(int );
static void createWindow(char *displayName, char *title);
static void redraw(void);
static int process_event(XEvent *event);
*/
