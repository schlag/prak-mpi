## Chapter 4 - Cellular Automata
This Chapter deals with some cellular automata simulation and the usage of MPI functions like
`MPI_Sendrecv` or `MPI_Gather`.

_Sequentiell_:

* `seq/caseq-slow` is - up to a certain degree - a quantitative model for the process of liquid separation.
* `seq/caseq-ultrafast` a tuned version of `caseq-slow` which is an order of magnitude faster that the naive one.

_Parallel_:

* `par/capar-slow` is parallelized version of `caseq-slow`
* `par/game-of-life` uses a different transition function to simulate Conway's Game of Life. It also contains code that, if used instead of random initialization, creates several gosper glider guns.
* `par/capar-slow-timing` is a version of `capar-slow` suitable for benchmarking.
* `par/capar-nonblocking` is a version of `capar-slow-timing` that uses non-blocking communication operations. All communication operations run in parallel.
* `par/capar-latency-hiding` is a version of `capar-nonblocking` that overlaps communication and calculations, thus hiding the communication latencies.
* `par/capar-decoupled.c` is  a special version of `par/capar-latency-hiding` that can perform k iterations without communicating. The program tests all k from 1 to 20 in order to find the sweet spot between reduced startups and increased number of re-computations.

## Commands
`xcinter <PEcount> <program> <#lines> <#iterations> <displayPeriod> <Display>`
(e.g. `xcinter 4 capar-slow 256 3000 1 $DISPLAY`). `#lines` has to be divisible by `PEcount`

## Todos

* ~~Array should be distributed evenly among all PEs.~~
* ~~It should be possible to start the program with different amounts of PEs (e.g. 1-6)~~
* ~~Use MPI_GATHER to collect state information for display~~
* Check out the fast version and try to parallelize it
* ~~If errors are visible in the results: make a screenshot and document error~~
* ~~Optional: Try Game of Life~~
* ~~Perform measurements (add synchronization barrier...)~~


## Additional Information

* MPI Standard -> MPI_Gather: `http://www.mpi-forum.org/docs/mpi22-report/node95.htm#Node95`
* Check out MPI_SendReceive for communication
* Parallel Initialization --> initParallelRandomLEcuyer
* Communication within boundary function
