/* (c) 1996,1997 Peter Sanders, Ingo Boesnach */
/* simulate a cellular automaton (serial version)
 * periodic boundaries
 *
 * use graphical display
 *
 * #1: Number of lines
 * #2: Number of iterations to be simulated
 * #3: Length of period (in iterations) between displayed states.
 *     0 means that no state is displayed.
 * #4: Name of X Display. E.g. "i90xxx.ira.uka.de:0.0"
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#include "bitmap.h"
#include "random.h"
#include "timing.h"

#include <assert.h>

/* horizontal size of the configuration */
#define XSIZE 256

/* "ADT" State and line of states (plus border) */
typedef char State;
typedef State Line[XSIZE + 2];
#define STATE MPI_CHAR

#define STEP1 42
#define STEP2 43

/* every how many simulation cycles shall the display be updated ? */
int displayPeriod;

/* determine random integer between 0 and n-1 */
#define randInt(n) ((int)(nextRandomLEcuyer() * n))

/* --------------------- debugging and I/O ---------------------------- */

/* display configuration stored in buf which consists
 * of lines lines on a window using bitmap.h
 */
static void displayConfig(Line *buf, int lines)
{  int i, x, y;

   /* pack data (-> 8 pixels/byte; necessary for bitmapDisplay) */
   i = 0;
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x+= 8, i++) {
         /*>^ Dirty trick: this reads up to 7 elements beyond
          *   a line boundary. This is OK because there is always
          *   legally accessible memory follwing if XSIZE >= 4
          */
         bitmapBuffer[i] = ((unsigned char)(buf[y][x + 0]) << 7) |
                           ((unsigned char)(buf[y][x + 1]) << 6) |
                           ((unsigned char)(buf[y][x + 2]) << 5) |
                           ((unsigned char)(buf[y][x + 3]) << 4) |
                           ((unsigned char)(buf[y][x + 4]) << 3) |
                           ((unsigned char)(buf[y][x + 5]) << 2) |
                           ((unsigned char)(buf[y][x + 6]) << 1) |
                           ((unsigned char)(buf[y][x + 7]) << 0);
	 /* printf("%d ",bitmapBuffer[i]); */
      }
      /* printf("\n"); */
   }

   bitOrder();
   bitmapDisplay();
}


/* --------------------- CA simulation -------------------------------- */

/* random starting configuration
 * 	Column boundary: 1
 * 	Line boundary: k
 */
static void initConfig(Line *buf, int lines, int rank, int numPEs, int k)
{  int x, y;

#ifdef STRIPETEST
 /* simple stripes that should form a "jumping fixpoint" */
 for (y = 1;  y <= lines;  y++) {
    for (x = 1;  x <= XSIZE;  x++) {
       buf[y][x] = 0;
       for(i = 30; i >= 0; i-=2)
          buf[y][x] |= (unsigned int)(((y)%2) << i);
    }
 }
#else
  initParallelRandomLEcuyer(424243,rank,numPEs);
   for (y = k;  y < lines+k;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         buf[y][x] = randInt(100) >= 50;
      }
   }
#endif
}

/* annealing rule from ChoDro96 page 34
 * the table is used to map the number of nonzero
 * states in the neighborhood to the new state
 */
static State anneal[10] = {0, 0, 0, 0, 1, 0, 1, 1, 1, 1};

/* a: pointer to array; x,y: coordinates; result: n-th element of anneal,
      where n is the number of neighbors */
#define transition(a, x, y) \
   (anneal[(a)[(y)-1][(x)-1] + (a)[(y)][(x)-1] + (a)[(y)+1][(x)-1] +\
           (a)[(y)-1][(x)  ] + (a)[(y)][(x)  ] + (a)[(y)+1][(x)  ] +\
           (a)[(y)-1][(x)+1] + (a)[(y)][(x)+1] + (a)[(y)+1][(x)+1]])

/**
 * k: width of the boundary that we have to send
 */
static void sendNonBlocking(MPI_Request request[], Line *buf, int lines, int myRank, int numPEs, int k)
{
   int commPartnerUp = (myRank + 1) % numPEs;
   int commPartnerDown = (myRank - 1 + numPEs) % numPEs;
   int msg_size = (XSIZE+2)*k;

   /* buf[0] first boundary line in buf
    * buf[k] first line with content
    * buf[k+lines-1] last line with content
    * buf[k+lines+k-1] last boundary line in buf */

    MPI_Isend(buf[k+lines-k], msg_size, MPI_CHAR, commPartnerUp, STEP1, MPI_COMM_WORLD, &request[0]);
    MPI_Irecv(buf[0], msg_size, MPI_CHAR, commPartnerDown, STEP1, MPI_COMM_WORLD, &request[1]);
 
    MPI_Isend(buf[k], msg_size, MPI_CHAR, commPartnerDown, STEP2, MPI_COMM_WORLD, &request[2]);
    MPI_Irecv(buf[k+lines], msg_size, MPI_CHAR, commPartnerUp, STEP2, MPI_COMM_WORLD, &request[3]);
}

/**
 * treat torus like boundary conditions
 * k: k width of the line-wise boundary. Right-left boundary is still 1
 */
static void boundary(Line *buf, int lines, int k)
{  int y;
   for (y = k;  y < lines+k;  y++) {
      /* copy rightmost column to the buffer column 0 */
      buf[y][0      ] = buf[y][XSIZE];

      /* copy leftmost column to the buffer column XSIZE + 1 */
      buf[y][XSIZE+1] = buf[y][1    ];
   }
}

/* make k simulation iterations with lines lines.
 * old configuration is in from, new one is written to to.
 *
 * k: num of iterations we can run locally without communication
 */
static void simulate(Line *from, Line *to, int lines, int myRank, int numPEs, int k)
{
   int x,y,i;
   MPI_Status status[4];
   MPI_Request request[4];
   int last_content_row = k+lines-1;

   boundary(from, lines, k);
   /* Send / receive the k first and last rows */
   sendNonBlocking(request, from, lines, myRank, numPEs, k); 

   /* Simulate the inner rows that do not depend on the boundary.
    *   frist row that can be simulated: k+1
    *   last row that can be simulated: k+lines-2
    */
   for (y = k+1;  y < k+lines-1;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         to[y][x  ] = transition(from, x  , y);
      }
   }

   /* Block until the communication has finished */
   MPI_Waitall(4, request, status);

   /* Use the received data to simulate the remaining rows, in order to finish
    * the first complete iteration. We never simulate the first/last boundary row.
    */
   for (i = 0; i < k; i++) {
      for (x = 1;  x <= XSIZE;  x++) {
         /* - boundary rows at the beginnig: from[1] - from[k-1]
          * - first row with actual content: from[k] */
         to[i+1][x  ] = transition(from, x  , i+1);
         /* - last row with actual content: from[k+lines-1]
          * - boundary rows at the end: from[k+lines] - from[k+lines+k-2] */
         to[last_content_row+i][x  ] = transition(from, x  , last_content_row+i);
      }
   } 

   /* Now we made excatly one iteration and have k-1 iterations left until we
    * have to commuicate again. Therefore we start with 2.
    *
    * The number of rows to simulate decreases per iteration, because
    * every following iteration will simulate one boundary row less.
    */
   for (i = 2; i < k; i++) {
      boundary(from, lines, k);
      for (y = i; y < last_content_row+k-i; y++) {
         for (x = 1; x <= XSIZE; x++) {
            to[y][x ] = transition(from, x , y);
         }
      }
   } 
}

/* --------------------- measurement ---------------------------------- */

int main(int argc, char** argv)
{  int lines, its;
   int i;
   double startTime, nowTime, lastTime, prunedAvg, *timing;
   Line *from, *to, *temp, *displayBuf;
   int myRank, numPEs, linesPerPE;
   int k;
   int measurementCounter;

   MPI_Init(&argc, &argv);

   if (argc != 5) {
     fprintf(stderr,"\nSynopsis: caseq-slow <lines> <iterations> "
	     "<dispPeriod> <display>\n");
     fprintf(stderr,"argc=%d\n",argc);
     for (i=0;i<argc;i++){
       fprintf(stderr,"argv[%d]=%s\n",i,argv[i]);
     }
     MPI_Abort(MPI_COMM_WORLD, 1);
   }

   lines = atoi(argv[1]);
   its   = atoi(argv[2]);
   displayPeriod = atoi(argv[3]);

   /* get PE count and local rank */
   MPI_Comm_size(MPI_COMM_WORLD, &numPEs);
   MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

   if (lines % numPEs == 0) {
       linesPerPE = lines / numPEs;
   } else {
       fprintf(stderr,"Number of lines (%d) cannot be divided by PE count (%d)\n",lines, numPEs);
       MPI_Abort(MPI_COMM_WORLD, 1);
   }
   /* only PE 0 should open up a window and allocate the needed extra memory */
   if (myRank == 0 && displayPeriod) {
     assert(displayBuf = malloc((lines + 2) * sizeof(Line)));
     bitmapInit(XSIZE, lines, argv[4], "BITMAP");
   }
   if (myRank == 0) {
     assert(timing = malloc(its * sizeof(double)));
   }

   /* simulation and measurement for different values of k */
   for (k = 1; k <= 20 && k <= linesPerPE; k++) {

     assert(from = malloc((linesPerPE + 2 * k) * sizeof(Line)));
     assert(to   = malloc((linesPerPE + 2 * k) * sizeof(Line)));

     initConfig(from, linesPerPE, myRank, numPEs, k);
     
     /* initial synchronization */
     MPI_Barrier(MPI_COMM_WORLD);

     if (myRank == 0){
       startTime = MPI_Wtime();
       lastTime = startTime;
       measurementCounter = 0;
     }

     /* measurement loop */
     /* each simulate call performs k iterations */
     for (i = 0;  i < its;  i += k) {
       simulate(from, to, linesPerPE, myRank, numPEs, k);
       temp = from;  from = to;  to = temp;

       if (myRank == 0){
         nowTime = MPI_Wtime();
         timing[measurementCounter] = (nowTime - lastTime) / k;
         lastTime = nowTime;
         measurementCounter = measurementCounter + 1;
       }

       if (displayPeriod && i % displayPeriod == 0) {
         MPI_Gather(from[k], linesPerPE*sizeof(Line), MPI_CHAR,
                    displayBuf[1], linesPerPE*sizeof(Line), MPI_CHAR,
                    0, MPI_COMM_WORLD);
         if (myRank == 0) {
           displayConfig(displayBuf, lines);
         }
       }
     }
     if (myRank == 0) {
       prunedAvg = pruned_average(timing, measurementCounter, 0.25);
       printf("%g %g %d # time[us] for one iteration, time[s] total, k \n", 1e6 * prunedAvg, MPI_Wtime() - startTime,k);
     }
     if (displayPeriod && myRank == 0) {
       puts("Press q or n in display window to exit.");
       eventLoop();
     }
     free(from);
     free(to);
   }
   

   closeAll();
   if (myRank == 0) {
     free(timing);
   }
   if (myRank == 0 && displayPeriod){
     free(displayBuf);
   }
   MPI_Finalize();
   return 0;
}
