/* (c) 1996,1997 Peter Sanders, Ingo Boesnach */
/* simulate a cellular automaton (serial version)
 * periodic boundaries
 *
 * use graphical display
 *
 * #1: Number of lines
 * #2: Number of iterations to be simulated
 * #3: Length of period (in iterations) between displayed states.
 *     0 means that no state is displayed.
 * #4: Name of X Display. E.g. "i90xxx.ira.uka.de:0.0"
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#include "bitmap.h"
#include "random.h"

#include <assert.h>

/* horizontal size of the configuration */
#define XSIZE 512

/* "ADT" State and line of states (plus border) */
typedef char State;
typedef State Line[XSIZE + 2];
#define STATE MPI_CHAR

#define STEP1 42
#define STEP2 43

/* every how many simulation cycles shall the display be updated ? */
int displayPeriod;

/* determine random integer between 0 and n-1 */
#define randInt(n) ((int)(nextRandomLEcuyer() * n))

/* --------------------- debugging and I/O ---------------------------- */

/* display configuration stored in buf which consists
 * of lines lines on a window using bitmap.h
 */
static void displayConfig(Line *buf, int lines)
{  int i, x, y;

   /* pack data (-> 8 pixels/byte; necessary for bitmapDisplay) */
   i = 0;
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x+= 8, i++) {
         /*>^ Dirty trick: this reads up to 7 elements beyond
          *   a line boundary. This is OK because there is always
          *   legally accessible memory follwing if XSIZE >= 4
          */
         bitmapBuffer[i] = ((unsigned char)(buf[y][x + 0]) << 7) |
                           ((unsigned char)(buf[y][x + 1]) << 6) |
                           ((unsigned char)(buf[y][x + 2]) << 5) |
                           ((unsigned char)(buf[y][x + 3]) << 4) |
                           ((unsigned char)(buf[y][x + 4]) << 3) |
                           ((unsigned char)(buf[y][x + 5]) << 2) |
                           ((unsigned char)(buf[y][x + 6]) << 1) |
                           ((unsigned char)(buf[y][x + 7]) << 0);
	 /* printf("%d ",bitmapBuffer[i]); */
      }
      /* printf("\n"); */
   }

   bitOrder();
   bitmapDisplay();
}


/* --------------------- CA simulation -------------------------------- */

/* random starting configuration */
static void initConfig(Line *buf, int lines, int rank, int numPEs)
{  int x, y;

#ifdef STRIPETEST
 /* simple stripes that should form a "jumping fixpoint" */
 for (y = 1;  y <= lines;  y++) {
    for (x = 1;  x <= XSIZE;  x++) {
       buf[y][x] = 0;
       for(i = 30; i >= 0; i-=2)
          buf[y][x] |= (unsigned int)(((y)%2) << i);
    }
 }
#else
  initParallelRandomLEcuyer(424243,rank,numPEs);
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         buf[y][x] = randInt(100) >= 50;
	 /*
	if(x>64 && x<=80)
	  buf[y][x]=1;
	else
	  buf[y][x]=0;
	 */
      }
   }
#endif
}

/* annealing rule from ChoDro96 page 34
 * the table is used to map the number of nonzero
 * states in the neighborhood to the new state
 */
static State anneal[10] = {0, 0, 0, 0, 1, 0, 1, 1, 1, 1};

/* a: pointer to array; x,y: coordinates; result: n-th element of anneal,
      where n is the number of neighbors */
#define transition(a, x, y) \
   (anneal[(a)[(y)-1][(x)-1] + (a)[(y)][(x)-1] + (a)[(y)+1][(x)-1] +\
           (a)[(y)-1][(x)  ] + (a)[(y)][(x)  ] + (a)[(y)+1][(x)  ] +\
           (a)[(y)-1][(x)+1] + (a)[(y)][(x)+1] + (a)[(y)+1][(x)+1]])

/* treat torus like boundary conditions */
static void boundary(Line *buf, int lines, int myRank, int numPEs)
{  int y;
   int commPartnerUp, commPartnerDown;
   MPI_Status status;

   for (y = 1;  y <= lines;  y++) {
      /* copy rightmost column to the buffer column 0 */
      buf[y][0      ] = buf[y][XSIZE];

      /* copy leftmost column to the buffer column XSIZE + 1 */
      buf[y][XSIZE+1] = buf[y][1    ];
   }

      commPartnerUp = (myRank + 1) % numPEs;
      commPartnerDown = (myRank - 1 + numPEs) % numPEs;
      MPI_Sendrecv(buf[lines], XSIZE+2, MPI_CHAR, commPartnerUp, STEP1,
                   buf[0], XSIZE+2, MPI_CHAR, commPartnerDown, STEP1,
                   MPI_COMM_WORLD, &status);

      MPI_Sendrecv(buf[1], XSIZE+2, MPI_CHAR, commPartnerDown, STEP2,
                   buf[lines+1], XSIZE+2, MPI_CHAR, commPartnerUp, STEP2,
                   MPI_COMM_WORLD, &status);
}

/* make one simulation iteration with lines lines.
 * old configuration is in from, new one is written to to.
 */
static void simulate(Line *from, Line *to, int lines, int myRank, int numPEs)
{
   int x,y;

   boundary(from, lines,myRank, numPEs);
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         to[y][x  ] = transition(from, x  , y);
      }
   }
}


/* --------------------- measurement ---------------------------------- */

int main(int argc, char** argv)
{  int lines, its;
   int i;
   double startTime;
   Line *from, *to, *temp, *displayBuf;
   int myRank, numPEs, linesPerPE;

   /* init */

   MPI_Init(&argc, &argv);

   if (argc != 5) {
     fprintf(stderr,"\nSynopsis: caseq-slow <lines> <iterations> "
	     "<dispPeriod> <display>\n");
     fprintf(stderr,"argc=%d\n",argc);
     for (i=0;i<argc;i++){
       fprintf(stderr,"argv[%d]=%s\n",i,argv[i]);
     }
     MPI_Abort(MPI_COMM_WORLD, 1);
   }

   lines = atoi(argv[1]);
   its   = atoi(argv[2]);
   displayPeriod = atoi(argv[3]);

/* --------------------- init for parallelization ---------------------------------- */
   /* get PE count and local rank */
   MPI_Comm_size(MPI_COMM_WORLD, &numPEs);
   MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

   /* line count should be divisible by PE count */
   if (lines % numPEs == 0) {
     linesPerPE = lines / numPEs;
     assert(from = malloc((linesPerPE + 2) * sizeof(Line)));
     assert(to   = malloc((linesPerPE + 2) * sizeof(Line)));
   } else {
     fprintf(stderr,"Number of lines (%d) cannot be divided by PE count (%d)\n",lines, numPEs);
     MPI_Abort(MPI_COMM_WORLD, 1);
   }

/* --------------------- standard code---------------------------------- */

   initConfig(from, linesPerPE, myRank, numPEs);

   /* only PE 0 should open up a window and allocate the needed extra memory */
   if (myRank == 0 && displayPeriod) {
     assert(displayBuf = malloc((lines + 2) * sizeof(Line)));
     bitmapInit(XSIZE, lines, argv[4], "BITMAP");
   }

   /* measurement loop */
   startTime = MPI_Wtime();

   for (i = 0;  i < its;  i++) {
     simulate(from, to, linesPerPE, myRank, numPEs);
     temp = from;  from = to;  to = temp;
     if (displayPeriod && i % displayPeriod == 0) {
       MPI_Gather(from[1], linesPerPE*sizeof(Line), MPI_CHAR,
                  displayBuf[1], linesPerPE*sizeof(Line), MPI_CHAR,
                  0, MPI_COMM_WORLD);
       if (myRank == 0) {
         displayConfig(displayBuf, lines);
       }
     }
   }

   printf("%fM total cells per second  %d %s\n",
         lines*XSIZE*its / (MPI_Wtime() - startTime)/1000, myRank,
         displayPeriod?"(but the states have been displayed)":"");

   if (displayPeriod && myRank == 0) {
     puts("Press q or n in display window to exit.");
     eventLoop();
   }

   closeAll();
   free(from);
   free(to);
   if(myRank == 0 && displayPeriod){
      free(displayBuf);
   }

   MPI_Finalize();
   return 0;
}
