This info documents how the pingpong-4b timing results were obtained:

* `xccc pingpong-4b.c -o pingpong-4b`: compile pingpong
* `xcbatch 9 pingpong-4b`: Perform batch run with 9 PEs
* Delete non-timing information from batchrun result file
* `./separateInput <modfied batchrun result filename>`: Split the result file in 8 different files that will be used by gnuplot.
* `gnuplot bandwidth_pingpong4.gpl`: Plot the results