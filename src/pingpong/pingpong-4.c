/* pingpong test with timing vor varying message sizes
 * June 4 97: version with pruned_average
 *
 * (c) 1996,1997 Peter Sanders
 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "timing.h"

/* A small debugging aid. Test condition "c" and print an error if it fails */
#define Assert(c) if(!(c)){printf(\
   "\nAssertion violation %s:%u:" #c "\n", __FILE__, __LINE__);}

/* Tags */

#define PING 42
#define PONG 17

/* buffer for message exchange */
#define BUFFERSIZE 1024 * 1024
double buffer[BUFFERSIZE];

/* each measurement should take about 1s */
#define NTrials(length) (1.0 / (50e-6 + sizeof(double) * (length) * 1e-8))

int main(int argc, char** argv)
{  int myId, numProcs, i, length, trials, PEtoCommunicateWith;
   double lastTime, nowTime, pa, *timing;
   MPI_Status status;

   MPI_Init(&argc, &argv);

   length = 1;

   MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
   MPI_Comm_rank(MPI_COMM_WORLD, &myId);
   Assert(numProcs > 8);

   timing = malloc(NTrials(1) * sizeof(double));
   Assert(timing != 0);

   for(PEtoCommunicateWith = 1; PEtoCommunicateWith <= numProcs-1; PEtoCommunicateWith++){
      if(myId == 0){
              printf("*** Communication With PE %d *** \n",PEtoCommunicateWith);
      }
      for (length = 1;  length <= BUFFERSIZE;  length *= 2) {
         trials = NTrials(length);
         /* if (myId == 0) printf("%d trials\n", trials); */
         MPI_Barrier(MPI_COMM_WORLD);
         if (myId == 0) {
            lastTime = MPI_Wtime();
            for (i = 0;  i < trials;  i++) {
               MPI_Send(buffer, length, MPI_DOUBLE, PEtoCommunicateWith, PING, MPI_COMM_WORLD);
               MPI_Recv(buffer, length, MPI_DOUBLE, PEtoCommunicateWith, PONG, MPI_COMM_WORLD, &status);
               nowTime = MPI_Wtime();
               timing[i] = nowTime - lastTime;
               lastTime = nowTime;
               /* printf("%f\n", timing[i]); */
            }
         } else if (myId == PEtoCommunicateWith){
            for (i = 0;  i < trials;  i++) {
               MPI_Recv(buffer, length, MPI_DOUBLE, 0, PING, MPI_COMM_WORLD, &status);
               MPI_Send(buffer, length, MPI_DOUBLE, 0, PONG, MPI_COMM_WORLD);
            }
         }

         if (myId == 0) {
            pa = pruned_average(timing, trials, 0.25);
            printf("%ld %g %g # length[B], time[us], bandwidth [MiB/s] %d trials\n",
                   length * sizeof(double), 1e6 * pa / 2.0,
                   2.0 * length * sizeof(double) / pa / 1024.0 / 1024.0,
                   trials);
         }
      }
   }
   MPI_Finalize();
   return 0;
}

