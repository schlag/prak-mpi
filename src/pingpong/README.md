Trivial MPI programms with simple unicast communication: One PE sends a PING to another PE and receives a PONG as a reply.

 * `pingpong-1.c`: Two communicating PEs. PE 0 sends n messages of size length*sizeof(double).
 * `pingpong-2.c`: Same as above, but wallclock time is measured until the PONG is returned. Bandwidth and pruned average timing for the n messages is reported.
 * `pingpong-3.c`: Ping pong test with timing for varying message sizes. The generated timing report can be plotted using `gnuplot bandwidth.gpl` and `gnuplot latency.gpl`. Among others, this illustrates the  special treatment of small messages by the MPI implementation.
 * `pingpong-4.c`: Ping pong over the network, which is enforced by communicating with a sufficiently large number of PEs (> 8).
