#include <stdio.h>
#include <stdlib.h>	
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include "matrix-util.h"
#include "matrix.h"
#include "timing.h"

#define DEBUG_ENABLED 0

#if DEBUG_ENABLED
#define DEBUG printf
#define DEBUG_DISPLAY showMatrix2
#define DEBUG_SLEEP sleep
#else
#define DEBUG(format, args...) ((void)0)
#define DEBUG_DISPLAY(mat, xdim, ydim) ((void)0)
#define DEBUG_SLEEP(myRank) ((void)0)
#endif

#define GRID_DIMENSION 2
#define X_DIMENSION	0
#define Y_DIMENSION 1

#define EPSILON 0.001

#define TYPE_GATHER 100
#define TYPE_SCATTER 101


/**
 * Create a GRID_DIMENSION-al Torus.
 */
void createCommGrid(int numPEs, MPI_Comm *comm_cart, int *myCoordinates) {
	int dims[GRID_DIMENSION];
	int periods[GRID_DIMENSION];
	int i, myRank;

  	/*  only entries that are 0 will be modified */
	for (i = 0; i < GRID_DIMENSION; i++) {
  		dims[i] = 0;
	}
  	MPI_Dims_create(numPEs, GRID_DIMENSION, dims);

  	/* treat grid as torus */
	for (i = 0; i < GRID_DIMENSION; i++) {
  		periods[i] = 1;
	}
	MPI_Cart_create(MPI_COMM_WORLD, GRID_DIMENSION, dims, periods, /*reorder*/ 1, comm_cart);

	MPI_Comm_rank(*comm_cart, &myRank);
	MPI_Cart_coords(*comm_cart, myRank, GRID_DIMENSION, myCoordinates);

	DEBUG("I'm PE %d and my coordinates in the %d-dimensional grid are (%d, %d).\n",
		myRank, GRID_DIMENSION, myCoordinates[0], myCoordinates[1]);
}

void createCommColumn(MPI_Comm comm_cart, MPI_Comm *comm_column) {
	int dimensions[GRID_DIMENSION];
    dimensions[0] = 1; /* dimension[0] equals column -> keep it */
    dimensions[1] = 0; 
    MPI_Cart_sub(comm_cart, dimensions, comm_column);
}

void createCommRow(MPI_Comm comm_cart, MPI_Comm *comm_row) {
	int dimensions[GRID_DIMENSION];
    dimensions[0] = 0; 
    dimensions[1] = 1; /* dimension[1] equals row -> keep it */
    MPI_Cart_sub(comm_cart, dimensions, comm_row);
}

void createCommMainDiagonale(MPI_Comm comm_cart, MPI_Comm *comm_diag, int myCoordinates[GRID_DIMENSION]) {
	int color = myCoordinates[X_DIMENSION] == myCoordinates[Y_DIMENSION];
	int rank = myCoordinates[X_DIMENSION];
	MPI_Comm_split(comm_cart, color, rank, comm_diag);
}

/**
 * For root PEs i = j (main diagonal)
 */ 
int getRoot(MPI_Comm comm, int coord) {
	int rootCoord[1], rootRank;
	rootCoord[0] = coord;

	MPI_Cart_rank(comm, rootCoord, &rootRank);
	return rootRank;
}

void gatherScatterSubMatrix(int type, MPI_Comm comm_cart, int num_blocks,
		int block_size, double *small_matrix, int n, double *total_matrix) {
		
	int i, j, coords[GRID_DIMENSION], rank;
	int *memory_offset, *msgcount;

	MPI_Datatype block_type, block_type_temp;

	memory_offset = malloc(num_blocks*num_blocks*sizeof(int));
	msgcount = malloc(num_blocks*num_blocks*sizeof(int));

	/* Setup displacement so that MPI knows where to find the one block for each PE */
	for (i = 0; i < num_blocks; i++) {
		coords[0] = i;
		for (j = 0; j < num_blocks; j++) {
			coords[1] = j;
			MPI_Cart_rank(comm_cart, coords, &rank);
			memory_offset[rank] = addr(i*block_size, j*block_size, n); 
			msgcount[rank] = 1;
		}
	}

	MPI_Type_vector(block_size, block_size, n, MPI_DOUBLE, &block_type_temp);
	MPI_Type_create_resized(block_type_temp, 0, sizeof(double), &block_type);

	if (type == TYPE_GATHER) {
		MPI_Gatherv(small_matrix, block_size*block_size, MPI_DOUBLE, total_matrix,
			msgcount, memory_offset, block_type, /*receiverRank*/ 0, comm_cart);
	} else if (type == TYPE_SCATTER) {
		MPI_Scatterv(total_matrix, msgcount, memory_offset, block_type,	small_matrix,
			block_size*block_size, MPI_DOUBLE, /*senderRank*/ 0, comm_cart);
	} else {
		assert(0);
	}
	
	MPI_Type_free(&block_type);
	free(memory_offset);
	free(msgcount);
}

void gatherScatterSubVector(int type, MPI_Comm comm_diag, int block_size,
	double *small_vector, double *total_vector) {
	
	if (type == TYPE_GATHER) {
		MPI_Gather(small_vector, block_size, MPI_DOUBLE, total_vector,
			block_size, MPI_DOUBLE, /*receiverRank*/ 0, comm_diag);
	} else if (type == TYPE_SCATTER) {
		MPI_Scatter(total_vector, block_size, MPI_DOUBLE, small_vector,
			block_size, MPI_DOUBLE, /*senderRank*/ 0, comm_diag);
	} else {
		assert(0);
	}
}

int main(int argc, char** argv) {

	int i,j;
	
	double *A, *x, *y, *input_matrix, *input_vector, *result, *reference;
	double *timing;
	double startTime, totalTime, prunedAvg, mflops;

	int n, numberOfIterations; /* dimension of matrices A, B, C */
	int num_blocks; /* number of blocks per dimension */
	int block_size; /* invariant: n = num_blocks * block_size */

	int myCoordinates[GRID_DIMENSION];
	int numPEs, myRank;
	MPI_Comm comm_cart, comm_row, comm_column, comm_diag;

	MPI_Init(&argc, &argv);

	if (argc != 3){
		fprintf(stderr,"\n Synopsis: vector <n> <numberOfIterations>!\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	n = atoi(argv[1]);
	numberOfIterations = atoi(argv[2]);

	MPI_Comm_size(MPI_COMM_WORLD, &numPEs);

  	num_blocks = sqrt(numPEs);
	block_size = n / num_blocks;

	if (fabs(n - num_blocks*block_size) > EPSILON) {
  		fprintf(stderr,"n (%d) cannot be divided by sqrt(PE count %d)\n", n, numPEs);
  		MPI_Abort(MPI_COMM_WORLD, 1);
  	}

	createCommGrid(numPEs, &comm_cart, myCoordinates);
	createCommRow(comm_cart, &comm_row);
	createCommColumn(comm_cart, &comm_column);
	createCommMainDiagonale(comm_cart, &comm_diag, myCoordinates);

	MPI_Barrier(comm_cart); /* for pretty debug output */

	MPI_Comm_rank(comm_cart, &myRank);

	/* y = A x */
	A = malloc(block_size*block_size*sizeof(double));
	x = malloc(block_size*sizeof(double));
	y = malloc(block_size*sizeof(double));

	if (myRank == 0) {
	 	input_matrix = malloc(n*n*sizeof(double));
	 	input_vector = malloc(n*sizeof(double));
	 	timing = malloc(numberOfIterations*sizeof(double));

	 	totalTime = 0;

		if (DEBUG_ENABLED ) {
			DEBUG("Structure: n=%d, p=%d, number_of_blocks_per_dimension=%d, elements_per_block_dimension=%d\n", n, numPEs, num_blocks, block_size);
			result = malloc(n*sizeof(double));
			reference = malloc(n*sizeof(double));
			vecInitSpecial(reference, n);
		}
	}

	if (myRank == 0) {
		DEBUG("Input Matrix A\n");
		matInitCreateType(input_matrix, n, SPECIAL_B);
		DEBUG_DISPLAY(input_matrix, n, n);
	}
	gatherScatterSubMatrix(TYPE_SCATTER, comm_cart, num_blocks, block_size, A, n, input_matrix);
	if (myRank == 0) {
		DEBUG("Input Vektor x\n");
		vecInitSpecial(input_vector, n);
		DEBUG_DISPLAY(input_vector, 1, n);
	}
	if (myCoordinates[X_DIMENSION] == myCoordinates[Y_DIMENSION]) {
		gatherScatterSubVector(TYPE_SCATTER, comm_diag, block_size, x, input_vector);
	}

	for (i = 0; i < numberOfIterations; i++) {
		startTime = MPI_Wtime();

		MPI_Bcast(x, block_size, MPI_DOUBLE, getRoot(comm_column, myCoordinates[Y_DIMENSION]),
			comm_column);

		matVecMult(block_size, A, x, y);
		
		MPI_Reduce(/*send buf*/ y, /*receive buf*/ x, block_size, MPI_DOUBLE, MPI_SUM,
			getRoot(comm_row, myCoordinates[X_DIMENSION]), comm_row);

		if (myRank == 0) {
			timing[i] = MPI_Wtime() - startTime;
			totalTime += timing[i];
		}
	}

	if (DEBUG_ENABLED) {
		if (myCoordinates[X_DIMENSION] == myCoordinates[Y_DIMENSION]) {
			gatherScatterSubVector(TYPE_GATHER, comm_diag, block_size, x, result);
		}

		if (myRank == 0) {
			fprintf(stdout,"Result:\n");
			DEBUG_DISPLAY(result, 1, n);

			assert(assertMatrixEquals2(1, n, reference, result));
			fprintf(stdout, "Test passed successfully!\n");

			free(result);
			free(reference);
		}
	}

	if (myRank == 0) {
    	prunedAvg = pruned_average(timing, numberOfIterations, 0.25);
    	mflops = (2*n*n-n)/(1e6*prunedAvg);
    	printf("%g %g %g # time[us] for one iteration, total time [s], MFLOPS \n", 1e6 * prunedAvg, totalTime, mflops);

		free(input_matrix);
		free(input_vector);
		free(timing);
	}
  	free(A);
  	free(x);
  	free(y);
	MPI_Finalize();
	return 0;
}
