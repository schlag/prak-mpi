#include <stdio.h>
#include <stdlib.h>	
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include "matrix-util.h"
#include "matrix.h"
#include "timing.h"

#define DEBUG_ENABLED 0  /*uncomment to enable DEBUG statements*/

#if DEBUG_ENABLED
#define DEBUG printf
#define DEBUG_DISPLAY showMatrix
#define DEBUG_SLEEP sleep
#else
#define DEBUG(format, args...) ((void)0)
#define DEBUG_DISPLAY(mant, n) ((void)0)
#define DEBUG_SLEEP(myRank) ((void)0)
#endif

#define GRID_DIMENSION 2
#define X_DIMENSION 1
#define Y_DIMENSION 0

#define EPSILON 0.001

#define TAG_DISTRIBUTE 42
#define TAG_CANNON_INIT 23
#define TAG_SHIFT_LEFT 13
#define TAG_SHIFT_UP 7

#define TYPE_GATHER 100
#define TYPE_SCATTER 101


/**
 * Create a GRID_DIMENSION-al Torus.
 */
void createCommGrid(int numPEs, MPI_Comm *comm_cart, int *myCoordinates) {
	int dims[GRID_DIMENSION];
	int periods[GRID_DIMENSION];
	int i, myRank;

  	/*  only entries that are 0 will be modified */
	for (i = 0; i < GRID_DIMENSION; i++) {
  		dims[i] = 0;
	}
  	MPI_Dims_create(numPEs, GRID_DIMENSION, dims);

  	/* treat grid as torus */
	for (i = 0; i < GRID_DIMENSION; i++) {
  		periods[i] = 1;
	}
	MPI_Cart_create(MPI_COMM_WORLD, GRID_DIMENSION, dims, periods, /*reorder*/ 1, comm_cart);

	MPI_Comm_rank(*comm_cart, &myRank);
	MPI_Cart_coords(*comm_cart, myRank, GRID_DIMENSION, myCoordinates);

	DEBUG("I'm PE %d and my coordinates in the %d-dimensional grid are (%d, %d).\n",
		myRank, GRID_DIMENSION, myCoordinates[0], myCoordinates[1]);
}

void alignMatrix(MPI_Comm comm_cart, int myCoordinates[GRID_DIMENSION], int block_size, double* matrix, int direction) {
	int sourceRank, destinationRank;
	int displacement;
	MPI_Status status;

	if (direction == X_DIMENSION) {
		/* rotate row i i times to the left */
		displacement = -myCoordinates[0]; 
	} else {
		/* rotate column j j times upwards */
		displacement = myCoordinates[1];
	}
	/* direction: coordinate dimension of shift
	 * displacement: shift length (> 0: upwards shift, < 0: downwards shift)
	 */
	MPI_Cart_shift(comm_cart, direction, displacement , &sourceRank, &destinationRank);
	MPI_Sendrecv_replace(matrix, block_size*block_size, MPI_DOUBLE,
		destinationRank, TAG_CANNON_INIT,
		sourceRank, TAG_CANNON_INIT, comm_cart, &status);
}

void rotateLeft(MPI_Comm comm_cart, int block_size, double *matrix) {
	int sourceRank, destinationRank;
	MPI_Status status;

	MPI_Cart_shift(comm_cart, X_DIMENSION, -1, &sourceRank, &destinationRank);
	MPI_Sendrecv_replace(matrix, block_size*block_size, MPI_DOUBLE,
		destinationRank, TAG_SHIFT_LEFT,
		sourceRank, TAG_SHIFT_LEFT, comm_cart, &status);
}

void rotateUp(MPI_Comm comm_cart, int block_size, double *matrix) {
	int sourceRank, destinationRank;
	MPI_Status status;

	MPI_Cart_shift(comm_cart, Y_DIMENSION, 1, &sourceRank, &destinationRank);
	MPI_Sendrecv_replace(matrix, block_size*block_size, MPI_DOUBLE,
		destinationRank, TAG_SHIFT_UP,
		sourceRank, TAG_SHIFT_UP, comm_cart, &status);
}

void gatherScatterSubMatrix(int type, MPI_Comm comm_cart, int num_blocks,
		int block_size, double *small_matrix, int n, double *total_matrix) {
		
	int i, j, coords[GRID_DIMENSION], rank;
	int *memory_offset, *msgcount;

	MPI_Datatype block_type, block_type_temp;

	memory_offset = malloc(num_blocks*num_blocks*sizeof(int));
	msgcount = malloc(num_blocks*num_blocks*sizeof(int));

	/* Setup displacement so that MPI knows where to find the one block for each PE */
	for (i = 0; i < num_blocks; i++) {
		coords[0] = i;
		for (j = 0; j < num_blocks; j++) {
			coords[1] = j;
			MPI_Cart_rank(comm_cart, coords, &rank);
			memory_offset[rank] = addr(i*block_size, j*block_size, n); 
			msgcount[rank] = 1;
		}
	}

	MPI_Type_vector(block_size, block_size, n, MPI_DOUBLE, &block_type_temp);
	MPI_Type_create_resized(block_type_temp, 0, sizeof(double), &block_type);

	if (type == TYPE_GATHER) {
		MPI_Gatherv(small_matrix, block_size*block_size, MPI_DOUBLE, total_matrix,
			msgcount, memory_offset, block_type, /*receiverRank*/ 0, comm_cart);
	} else if (type == TYPE_SCATTER) {
		MPI_Scatterv(total_matrix, msgcount, memory_offset, block_type,	small_matrix,
			block_size*block_size, MPI_DOUBLE, /*senderRank*/ 0, comm_cart);
	} else {
		assert(0);
	}
	
	MPI_Type_free(&block_type);
	free(memory_offset);
	free(msgcount);
}

int main(int argc, char** argv) {

	int i,j;
	
	double *A, *B, *C, *input_matrix, *result, *reference;
	double *timing;
	double startTime, totalTime, prunedAvg;

	int n,numberOfIterations; /* dimension of matrices A, B, C */
	int num_blocks; /* number of blocks per dimension */
	int block_size; /* invariant: n = num_blocks * block_size */

	int myCoordinates[GRID_DIMENSION];
	int numPEs, myRank;
	MPI_Comm comm_cart;

	MPI_Init(&argc, &argv);

	if (argc != 3){
		fprintf(stderr,"\n Synopsis: cannon <n> <numberOfIterations>!\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	n = atoi(argv[1]);
	numberOfIterations = atoi(argv[2]);

	MPI_Comm_size(MPI_COMM_WORLD, &numPEs);

  	num_blocks = sqrt(numPEs);
	block_size = n / num_blocks;

	if (fabs(n - num_blocks*block_size) > EPSILON) {
  		fprintf(stderr,"n (%d) cannot be divided by sqrt(PE count %d)\n", n, numPEs);
  		MPI_Abort(MPI_COMM_WORLD, 1);
  	}

	createCommGrid(numPEs, &comm_cart, myCoordinates);
	MPI_Barrier(comm_cart); /* for pretty debug output */

	MPI_Comm_rank(comm_cart, &myRank);

	A = malloc(block_size*block_size*sizeof(double));
	B = malloc(block_size*block_size*sizeof(double));
	C = malloc(block_size*block_size*sizeof(double));

	if (myRank == 0) {
	 	input_matrix = malloc(n*n*sizeof(double));
	 	timing = malloc(numberOfIterations*sizeof(double));

	 	totalTime = 0;

		if (DEBUG_ENABLED ) {
			DEBUG("Structure: n=%d, p=%d, number_of_blocks_per_dimension=%d, elements_per_block_dimension=%d\n", n, numPEs, num_blocks, block_size);
			result = malloc(n*n*sizeof(double));
			reference = malloc(n*n*sizeof(double));
			matInitCreateType(reference, n, SPECIAL_A);
		}
	}

	for (i = 0; i < numberOfIterations; i++) {
		
		matInitZero(C, block_size);
		if (myRank == 0) {
			DEBUG("Input Matrix A\n");
			matInitCreateType(input_matrix, n, SPECIAL_A);
			DEBUG_DISPLAY(input_matrix, n);
		}
		gatherScatterSubMatrix(TYPE_SCATTER, comm_cart, num_blocks, block_size, A, n, input_matrix);
		if (myRank == 0) {
			DEBUG("Input Matrix B\n");
			matInitCreateType(input_matrix, n, SPECIAL_B);
			DEBUG_DISPLAY(input_matrix, n);
		}
		gatherScatterSubMatrix(TYPE_SCATTER, comm_cart, num_blocks, block_size, B, n, input_matrix);
		
		startTime = MPI_Wtime();

		/* initial alignment needed by cannon's algorithm */
		alignMatrix(comm_cart, myCoordinates, block_size, A, X_DIMENSION);
		alignMatrix(comm_cart, myCoordinates, block_size, B, Y_DIMENSION);
	
		/* actual matrix multiplication */
		matMatMultAdd(block_size, A, B, C);
		for (j = 1; j < num_blocks; ++j) {
			rotateLeft(comm_cart, block_size, A);
			rotateUp(comm_cart, block_size, B);
			matMatMultAdd(block_size, A, B, C);
		}

		if (myRank == 0) {
			timing[i] = MPI_Wtime() - startTime;
			totalTime += timing[i];
		}

		if (DEBUG_ENABLED) {
			gatherScatterSubMatrix(TYPE_GATHER, comm_cart, num_blocks, block_size, C, n, result);
	
			if (myRank == 0) {
				fprintf(stdout,"Result:\n");
				DEBUG_DISPLAY(result,n);
	
				assert(assertMatrixEquals(n, reference, result));
				fprintf(stdout, "Test passed successfully!\n");
	
				//free(result);
				//free(reference);
			}
		}
	}

	if (myRank == 0) {
    	prunedAvg = pruned_average(timing, numberOfIterations, 0.25);
    	printf("%g %g # time[us] for one iteration, total time [s] \n", 1e6 * prunedAvg, totalTime);
    }

	if (myRank == 0) free(input_matrix);
  	free(A);
  	free(B);
  	free(C);
	MPI_Finalize();
	return 0;
}
