#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix-util.h"

int main(int argc, char** argv)
{
	double *test;
	int n;

	if (argc != 2){
		fprintf(stderr,"\n Parameter <n> is missing!\n");
		return 1;
	}

	n = atoi(argv[1]);
	test = malloc(n*n*sizeof(double));

	printf("zero matrix:\n");
	matInitZero(test,n);
	showMatrix(test,n);

	printf("identity matrix:\n");
	matInitIdentity(test,n);
	showMatrix(test,n);

	printf("A:\n");
	matInitSpecialA(test,n);
	showMatrix(test,n);

	printf("B:\n");
	matInitSpecialB(test,n);
	showMatrix(test,n);

	free(test);
	return 0;
}
