#include <stdio.h>
#include <stdlib.h>	
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include "matrix-util.h"
#include "matrix.h"
#include "timing.h"

#define DEBUG_ENABLED 0

#if DEBUG_ENABLED
#define DEBUG printf
#define DEBUG_DISPLAY showMatrix2
#define DEBUG_SLEEP sleep
#else
#define DEBUG(format, args...) ((void)0)
#define DEBUG_DISPLAY(mat, xdim, ydim) ((void)0)
#define DEBUG_SLEEP(myRank) ((void)0)
#endif

#define GRID_DIMENSION 2
#define X_DIMENSION	0
#define Y_DIMENSION 1

#define EPSILON 0.001

#define TYPE_GATHER 100
#define TYPE_SCATTER 101

int main(int argc, char** argv) {

	int i;
	
	double *A, *x, *y, *reference, *tmp;
	double *timing;
	double startTime, totalTime, prunedAvg, mflops;

	int n, numberOfIterations; /* dimension of matrices A, B, C */

	MPI_Init(&argc, &argv);

	if (argc != 3){
		fprintf(stderr,"\n Synopsis: vector <n> <numberOfIterations>!\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	n = atoi(argv[1]);
	numberOfIterations = atoi(argv[2]);

	/* y = A x */
	A = malloc(n*n*sizeof(double));
	x = malloc(n*sizeof(double));
	y = malloc(n*sizeof(double));
	
	timing = malloc(numberOfIterations*sizeof(double));
	totalTime = 0;

	if (DEBUG_ENABLED ) {
		reference = malloc(n*sizeof(double));
		vecInitSpecial(reference, n);
	}

	matInitCreateType(A, n, SPECIAL_B);
	DEBUG_DISPLAY(A, n, n);
	
	DEBUG("Input Vektor x\n");
	vecInitSpecial(x, n);
	DEBUG_DISPLAY(x, 1, n);

	for (i = 0; i < numberOfIterations; i++) {
		startTime = MPI_Wtime();

		matVecMult(n, A, x, y);

		tmp = x;
		x = y;
		y = tmp;


		timing[i] = MPI_Wtime() - startTime;
		totalTime += timing[i];
	}

	if (DEBUG_ENABLED) {
		fprintf(stdout,"Result:\n");
		DEBUG_DISPLAY(y, 1, n);

		assert(assertMatrixEquals2(1, n, reference, y));
		fprintf(stdout, "Test passed successfully!\n");

		free(reference);
	}

    prunedAvg = pruned_average(timing, numberOfIterations, 0.25);
    mflops = (2*n*n-n)/(1e6*prunedAvg);
    printf("%g %g %g # time[us] for one iteration, total time [s], MFLOPS \n", 1e6 * prunedAvg, totalTime, mflops);

	free(timing);
  	free(A);
  	free(x);
  	free(y);
	MPI_Finalize();
	return 0;
}
