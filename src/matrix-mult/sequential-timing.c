#include <stdio.h>
#include <stdlib.h>
#include "matrix-util.h"
#include "matrix.h"
#include <mpi.h>
#include "timing.h"


/**
 * Calculate C = A B, via sum of C_ij = A_ik * B_kj
 */
void matMatMultSequential(int n, double *A, double *B, double *C){
	int i, j; /* Coordinates with regard to the result matrix C */
	int k;
	double sum;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			sum = 0;
			for (k = 0; k < n; k++) {
				sum += A[addr(i,k,n)] * B[addr(k,j,n)];
			}
			C[addr(i,j,n)] = sum;
		}
	}
}

int main(int argc, char** argv) {
	double *A, *B, *C;
	int n, i, numberOfIteration;
	double *timing, startTime, avg;
	
	MPI_Init(&argc, &argv);

	if (argc != 3){
		fprintf(stderr,"\n Parameter <n> is missing!\n");
		return 1;
	}

	n = atoi(argv[1]);
	numberOfIteration = atoi(argv[2]); 

	A = malloc(n*n*sizeof(double));
	B = malloc(n*n*sizeof(double));
	C = malloc(n*n*sizeof(double));
	timing = malloc(numberOfIteration*sizeof(double));

	matInitSpecialA(A, n);	
	matInitSpecialB(B, n);

	for (i = 0; i < numberOfIteration; i++) {
		matInitZero(C, n);
		startTime = MPI_Wtime();
		matMatMultSequential(n, A, B, C);
		timing[i] = MPI_Wtime() - startTime;
	}
	
	avg = pruned_average(timing, numberOfIteration, 0.25);
	printf("%g # time for one iteration [us]\n", 1e6*avg);

	free(A);
	free(B);
	free(C);
	MPI_Finalize();
	return 0;
}
