## Chapter 5 - Linear Algebra

Main objective of this chapter is to implement a parallel matrix and parallel matrix-vector multiplication.
The chapter also deals with collective communication operations such as gather, broadcast and reduce.

_Matrix-Multiplication:_

 * `sequential.c`: A custom O(n^3) matrix multiplication.
 * `sequential-library.c`: A helper program used to time a highly-tuned library implementation of a sequential matrix multiplication. The timing is obtained in order to establish a baseline for the _absolut_ speedup of our parallel implementation.
 * `cannon.c`: Implementation of the parallel cannon matrix multiplication.
 * `matrix-util.c`: Various helper functions for the construction and comparison of matrixes.

_Matrix-Vector-Multiplication:_

 * `vector-sequential.c`: Library program used as a baseline for our _absolut_ speedup. 
 * `vector.c`: Parallel matrix-vector multiplication which makes heavy use of broadcast and reduce.
