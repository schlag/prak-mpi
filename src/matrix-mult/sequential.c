#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix-util.h"
#include <assert.h>

/**
 * Calculate C = A B, via sum of C_ij = A_ik * B_kj
 */
void matMatMultSequential(int n, double *A, double *B, double *C){
	int i, j; /* Coordinates with regard to the result matrix C */
	int k;
	double sum;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			sum = 0;
			for (k = 0; k < n; k++) {
				sum += A[addr(i,k,n)] * B[addr(k,j,n)];
			}
			C[addr(i,j,n)] = sum;
		}
	}
}

int main(int argc, char** argv) {
	double *A, *B, *C, *reference;
	int n;

	if (argc != 2){
		fprintf(stderr,"\n Parameter <n> is missing!\n");
		return 1;
	}

	n = atoi(argv[1]);
	A = malloc(n*n*sizeof(double));
	B = malloc(n*n*sizeof(double));
	C = malloc(n*n*sizeof(double));
	reference = malloc(n*n*sizeof(double));

	/**
	 * Test the test infrastructure itself
	 */
	matInitIdentity(A, n);	
	matInitIdentity(B, n);

	assert(assertMatrixEquals(n, A, B));
	A[addr(0,0,n)] = 2;
	assert(!assertMatrixEquals(n, A, B));

	/**
	 * 1) Multiplication of identity should yield identity 
	 */
	matInitIdentity(A, n);	
	matInitIdentity(B, n);
	matInitIdentity(reference, n);

	matMatMultSequential(n, A, B, C);
	showMatrix(C, n);
	assert(assertMatrixEquals(n, reference, C));

	/**
	 * 2) Multiplication with zero should yield zero
	 */
	matInitIdentity(A, n);	
	matInitZero(B, n);
	matInitZero(reference, n);

	matMatMultSequential(n, A, B, C);
	showMatrix(C, n);
	assert(assertMatrixEquals(n, reference, C));

	/**
	 * 3) Multiplication with identity should yield the same
	 */
	matInitIdentity(A, n);	
	matInitSpecialB(B, n);
	matInitSpecialB(reference, n);

	matMatMultSequential(n, A, B, C);
	showMatrix(C, n);
	assert(assertMatrixEquals(n, reference, C));

	/**
	 * 4) Multiplication of the special matrixes A and B
	 */
	matInitSpecialA(A, n);	
	matInitSpecialB(B, n);
	matInitSpecialA(reference, n);

	matMatMultSequential(n, A, B, C);
	showMatrix(C, n);
	assert(assertMatrixEquals(n, reference, C));

	free(A);
	free(B);
	free(C);
	free(reference);

	fprintf(stdout, "All tests passed successfully!\n");
	return 0;
}
