#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "matrix-util.h"


/***********************************
 * Debug and Test Utils
 ***********************************/

#define EPSILON 0.0001

int assertMatrixEquals(int n, double *reference, double *actual) {
	return assertMatrixEquals2(n, n, reference, actual);
}

int assertMatrixEquals2(int xdim, int ydim, double *reference, double *actual) {
	int i, j;
	for (i = 0; i < xdim; i++) {
		for (j = 0; j < ydim; j++) {
			if (fabs(reference[addr(i,j,xdim)] - actual[addr(i,j,xdim)]) > EPSILON) {
				return 0;
			}
		}
	}
	return 1;
}

void showMatrix(double *mat, int n) {
	showMatrix2(mat, n, n);
}

void showMatrix2(double *mat, int xdim, int ydim) {
	int i;
	for (i = 0; i < xdim*ydim; i++) {
		printf("\t%.3g ", mat[i]);
		if ( (i+1) % xdim == 0 ) {
			printf("\n");
		}
	}
	printf("\n");	
}


/***********************************
 * Various Init Functions
 ***********************************/

void matInitCreateType(double *mat, int n, enum matrixkind kind) {

	switch (kind) {
	case ENUM:
		matInitEnumerate(mat, n);
	 	break;
	case IDENTITY:
		matInitIdentity(mat, n);
		break;
	case ZERO:
		matInitZero(mat, n);
		break;
	case SPECIAL_A:
		matInitSpecialA(mat, n);
		break;
	case SPECIAL_B:
		matInitSpecialB(mat, n);
		break;
	}
}

void matInitEnumerate(double *mat, int n) {
	int i;
	for (i = 0; i < n*n; ++i) {
		mat[i]=i;
	}
}

void matInitZero(double *mat, int n) {
	matInitZero2(mat, n, n);
}

void matInitZero2(double *mat, int xdim, int ydim) {
	int i;
	for (i = 0; i < xdim*ydim; ++i) {
		mat[i]=0;
	}
}

void matInitIdentity(double *mat, int n) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			mat[addr(i,j,n)] = i == j;
		}
	}
}

/* mat[i][j] = i / (j+1) */
void matInitSpecialA(double *mat, int n) {
	int i, j;
	for (i = 0; i < n; i++){
		for (j = 0; j <n ; j++) {
			mat[addr(i,j,n)] = i / (j + 1.0);
		}
	}	
}

/* mat[i][j] = (i+1) / (n*(j+1)) */
void matInitSpecialB(double *mat, int n) {
	int i, j;
	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++) {
			mat[addr(i,j,n)] = (i + 1.0) / (n * (j + 1.0));
		}
	}
}

/* vec[i] = i+1 */
void vecInitSpecial(double *vec, int n) {
	int i;
	for (i = 0; i < n; i++){
		vec[i] = i + 1.0;
	}
}
