## Chapter 7 - Loadbalancing
This Chapter deals with various algorithms for load balancing. The algorithms are used to parallelize an irregular problem, the computation of the Mandelbrot set.

_Sequentiell_:

* `apfelseq.c` implements a sequential computation of the entire set. A second PE is involved in plotting the results.

_Parallel_:

* `apfelpar-static.c` implements two static means for distributing work among worker PEs: stripe-based decomposition and scatter decomposition.
* `apfelpar-static-rand.c` a version of `apfelpar-static.c` which does not distribute work according to a fixed scheme, but according to a random permuation.
* `apfelpar-masterworker.c` a version of `apfelseq.c` in which a master PE dynamically dispatches work to a group of worker PEs.
* `apfelpar-randompolling.c` a version of `apfelseq.c` which implements dynamic loadbalancing without a designated master PE. Work is distributed via work-stealing, based on random-polling of other PEs.

