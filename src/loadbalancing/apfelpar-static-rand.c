/* (c) 1997 Ingo Boesnach */
/* mandelbrot set (serial version)
 * argv[1]: smallest real part considered
 * argv[2]: smallest imaginary part considered
 * argv[3]: extent of square area considered --> a_0
 * argv[4]: resolution                       --> n
 * argv[5]: maximum number of interations    --> m_max
 * argv[6]: display 
 * e.g. "poe a.out -procs 2 -1.5 -1 2 400 200 i90s14.ira.uka.de:0.0"
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#include <math.h>
#include "mpi.h"
#include "bitmap.h"
#include "random.h"
#include "timing.h"

#define DISPLAY   1

int iProc, nProc;

/*********** Mandelbrot specific code ******************************/

#define LARGE   2.0

/* determine random integer between 0 and n-1 */
#define randInt(n) ((int)(nextRandomLEcuyer() * n))

double complex z0;
double extent;  /* a_0 in the book         */
int resolution; /* n in the book           */
double step;    /* a_0/n                   */
int maxiter;    /* m_max in the book       */
int withgraph;  /* Flag: display results ? */

/* perform a single Mandelbrot iteration starting at c and return
 * number of iterations
 */
int isInMandebrotSet(int pos) {  
  int iter;
  double complex c = z0 + (double)(pos % resolution) * step + I * (double)(pos / resolution) * step;
  double complex z = c;
  for (iter = 1;  iter < maxiter && abs(z) <= LARGE;  iter++) z = z*z + c;
  return iter == maxiter;
}

/*********** Server Code **********************/

/* From each worker, get an interval size and a sequence
 * of integers, each encoding one element of the Mandelbrot set.
 */
void server(char *display) {
  int length;
  MPI_Status status;
  int recvBuffer[resolution * resolution];

  if (withgraph) bitmapInit(resolution, resolution, display, "Mandelbrot");
  
  for (int receiveCount=0; receiveCount < nProc-1; receiveCount++) {
    MPI_Recv(recvBuffer, resolution*resolution, MPI_INT, MPI_ANY_SOURCE, DISPLAY, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_INT, &length);

    for (int i = 0;  i < length;  i++) {
      int x = recvBuffer[i] % resolution;
      int y = recvBuffer[i] / resolution;
      if (withgraph) bitmapSetPixel(x, resolution - y - 1);
      /* remember: (0,0) of bitmap corresponds to complex (z0 + i*n) */
    }
    if (withgraph) {
      printf("plotting %d pixels from PE %i\n", length, status.MPI_SOURCE);
      bitmapDisplay();
    }
  }
}

/*********** Worker Code **********************/

static long ipow(int base, int exp) {
    long result = 1;
    while (exp > 0) {
        if (exp & 1) {
          result *= base;
        }
        exp >>= 1;
        base *= base;
    }
    return result;
}

static long powdiv(long pow_a, int a, int d) {
  return ((pow_a-1)/(a-1)) * d;
}
static int nextX(int X, long pow_a, long pow_div, int N){
  return (pow_a * X + pow_div) % N;
}

static int firstX(int a, int d, int N) {
  long pow_a = ipow(a, iProc-1);
  long pow_div = powdiv(pow_a, a, d);
  return nextX(randInt(N), pow_a, pow_div, N);
}

/* work on an interval of integers representing candidate elements
 * of the Mandelbrot set.
 */
void worker() { 
  int totalWork = resolution*resolution;

  int d = randInt(10);
  d = (d % 2 == 0) ? d + 1 : d; // d has to be odd
  int a = 4 * (1 + randInt(4)) + 1;
  int N = ipow(2, ceil(log2(totalWork))); // next power of 2
  int X = firstX(a, d, N);

  if (withgraph) printf("PE %i, firstX=%i, N=%i, d=%i, a=%i \n", iProc,X,N,d,a);
  /* contains interval size and a sequence of integers encoding one element of 
   * the Mandelbrot set each 
   */
  int result[totalWork];
  int elem = 0;

  long pow_a = ipow(a, nProc-1);
  long pow_div = powdiv(pow_a, a, d);

  for (int k = 0; k < N/(nProc-1);  k++) {
    X = nextX(X, pow_a, pow_div, N);

    if (isInMandebrotSet(X) && X < totalWork) {
      /* we assume to have an element of the Mandelbrot set here */ 
      result[elem] = X;
      elem++;
    }
  }
  /* send results to server */
  MPI_Send(result, elem, MPI_INT, 0, DISPLAY, MPI_COMM_WORLD);  
}

/*********************************************************************/

int main(int argc, char **argv) {
  
  MPI_Init(&argc, &argv);
  assert(argc >= 7);
  assert(argc <= 8);

  float z0Real   = atof(argv[1]);
  float z0Img    = atof(argv[2]);
  z0 = z0Real + I * z0Img ;
  extent         = atof(argv[3]);   // a_0
  resolution     = atoi(argv[4]);   // n
  step = extent / resolution; 
  maxiter        = atoi(argv[5]);   // m_max
  int iterations = atoi(argv[6]);
  withgraph = (argc == 7) ? 0 : 1;
  char *display  = argv[7];

  if (resolution % (nProc-1) != 0 ) {
    fprintf(stderr,"Work can't be distirbuted equally among %i worker PEs\n", nProc-1);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  assert(nProc >= 2);

  double timings[iterations];
  initRandomLEcuyer(424243);

  for (int i = 0;  i < iterations;  i++) {
    MPI_Barrier(MPI_COMM_WORLD); /* synchronize for accurate timing */

    if (iProc == 0) {
      double startTime = MPI_Wtime();
      server(display);
      timings[i]= MPI_Wtime() - startTime;
    } else {
      worker(); 
    }
  }
  if (withgraph) getchar();

  if (iProc == 0) {
      double prunedAvg = pruned_average(timings, iterations, 0.25);
      printf("%i %f %f %g %i %i %i %g # PEs, z0_real, z0_img, a0, n, m_max, iterations time[us] for one iteration \n",
        nProc, z0Real, z0Img, extent, resolution, maxiter, iterations, 1e6 * prunedAvg);
   }

  MPI_Finalize();
  return 0;
}
