/* (c) 1997 Ingo Boesnach */
/* mandelbrot set (serial version)
 * argv[1]: smallest real part considered
 * argv[2]: smallest imaginary part considered
 * argv[3]: extent of square area considered --> a_0
 * argv[4]: resolution                       --> n
 * argv[5]: maximum number of interations    --> m_max
 * argv[6]: display 
 * e.g. "poe a.out -procs 2 -1.5 -1 2 400 200 i90s14.ira.uka.de:0.0"
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#include "mpi.h"
#include "bitmap.h"
#include "random.h"
#include "timing.h"

#define DISPLAY   1
#define STATIC_STRIPE 0 // 1: static stripe; 0: scatter decomposition

int iProc, nProc;
typedef struct {int start; int end; int step;} Work;

/*********** Mandelbrot specific code ******************************/

#define LARGE   2.0

double complex z0;
double extent;  /* a_0 in the book         */
int resolution; /* n in the book           */
double step;    /* a_0/n                   */
int maxiter;    /* m_max in the book       */
int withgraph;  /* Flag: display results ? */

/* perform a single Mandelbrot iteration starting at c and return
 * number of iterations
 */
int isInMandebrotSet(int pos) {  
  int iter;
  double complex c = z0 + (double)(pos % resolution) * step + I * (double)(pos / resolution) * step;
  double complex z = c;
  for (iter = 1;  iter < maxiter && abs(z) <= LARGE;  iter++) z = z*z + c;
  return iter == maxiter;
}

/*********** Server Code **********************/

/* From each worker, get an interval size and a sequence
 * of integers, each encoding one element of the Mandelbrot set.
 */
void server(char *display) {
  int length;
  MPI_Status status;
  int recvBuffer[resolution * resolution];

  if (withgraph) bitmapInit(resolution, resolution, display, "Mandelbrot");
  
  for (int receiveCount=0; receiveCount < nProc-1; receiveCount++) {
    MPI_Recv(recvBuffer, resolution*resolution, MPI_INT, MPI_ANY_SOURCE, DISPLAY, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_INT, &length);

    for (int i = 1;  i < length;  i++) {
      int x = recvBuffer[i] % resolution;
      int y = recvBuffer[i] / resolution;
      if (withgraph) bitmapSetPixel(x, resolution - y - 1);
      /* remember: (0,0) of bitmap corresponds to complex (z0 + i*n) */
    }
    if (withgraph) {
      printf("plotting %d pixels out of %d\n", length-1, recvBuffer[0]);
      bitmapDisplay();
    }
  }
}

/*********** Worker Code **********************/

/* work on an interval of integers representing candidate elements
 * of the Mandelbrot set.
 */
void worker(Work work, int workPerPE) { 
  /* contains interval size and a sequence of integers encoding one element of 
   * the Mandelbrot set each 
   */
  int result[workPerPE+1];
  int elem = 1;

  if (withgraph) printf("starting work in range from %d to %d with step %d\n", work.start, work.end, work.step);

  for (int i = work.start; i <= work.end;  i += work.step) {
    if (isInMandebrotSet(i)) {
      /* we assume to have an element of the Mandelbrot set here */
      result[elem] = i;
      elem++;
    }
  }
  /* send results to server */
  result[0] = workPerPE;
  MPI_Send(result, elem, MPI_INT, 0, DISPLAY, MPI_COMM_WORLD);  
}

void dispatchWorker() {
  Work work;
  int workPerPE = (resolution / (nProc-1)) * resolution;

  if (STATIC_STRIPE) {
    work.start = (iProc-1) * workPerPE; 
    work.end = work.start + workPerPE-1;
    work.step = 1;
  } else { // scatter decomposition
    work.start = iProc-1;
    work.end = resolution * resolution;
    work.step = nProc-1;
  }
  worker(work, workPerPE); 
}

/*********************************************************************/

int main(int argc, char **argv) {
  
  MPI_Init(&argc, &argv);
  assert(argc >= 7);
  assert(argc <= 8);

  float z0Real   = atof(argv[1]);
  float z0Img    = atof(argv[2]);
  z0 = z0Real + I * z0Img ;
  extent         = atof(argv[3]);   // a_0
  resolution     = atoi(argv[4]);   // n
  step = extent / resolution; 
  maxiter        = atoi(argv[5]);   // m_max
  int iterations = atoi(argv[6]);
  withgraph = (argc == 7) ? 0 : 1;
  char *display  = argv[7];

  if (resolution % (nProc-1) != 0 ) {
    fprintf(stderr,"Work can't be distirbuted equally among %i worker PEs\n", nProc-1);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  assert(nProc >= 2);

  double timings[iterations];

  for (int i = 0;  i < iterations;  i++) {
    MPI_Barrier(MPI_COMM_WORLD); /* synchronize for accurate timing */

    if (iProc == 0) {
      double startTime = MPI_Wtime();
      server(display);
      timings[i]= MPI_Wtime() - startTime;
    } else {
      dispatchWorker();
    }
  }
  if (withgraph) getchar();

  if (iProc == 0) {
      double prunedAvg = pruned_average(timings, iterations, 0.25);
      printf("%i %f %f %g %i %i %i %g # PEs, z0_real, z0_img, a0, n, m_max, iterations time[us] for one iteration \n",
        nProc, z0Real, z0Img, extent, resolution, maxiter, iterations, 1e6 * prunedAvg);
   }

  MPI_Finalize();
  return 0;
}
