/* (c) 1997 Ingo Boesnach */
/* mandelbrot set (serial version)
 * argv[1]: smallest real part considered
 * argv[2]: smallest imaginary part considered
 * argv[3]: extent of square area considered --> a_0
 * argv[4]: resolution                       --> n
 * argv[5]: maximum number of interations    --> m_max
 * argv[6]: how much data n^2/k to assign per worker request
 * argv[7]: display 
 * e.g. "poe a.out -procs 2 -1.5 -1 2 400 200 10 i90s14.ira.uka.de:0.0"
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#include "mpi.h"
#include "bitmap.h"
#include "random.h"
#include "timing.h"

#define START 0
#define END 1

#define RESULT 41
#define WORK 42
#define DONE 43

int iProc, nProc;

/*********** Mandelbrot specific code ******************************/

#define LARGE   2.0

double complex z0;
double extent;  /* a_0 in the book         */
int resolution; /* n in the book           */
double step;    /* a_0/n                   */
int maxiter;    /* m_max in the book       */
int withgraph;  /* Flag: display results ? */

/* perform a single Mandelbrot iteration starting at c and return
 * number of iterations
 */
int isInMandebrotSet(int pos) {  
  int iter;
  double complex c = z0 + (double)(pos % resolution) * step + I * (double)(pos / resolution) * step;
  double complex z = c;
  for (iter = 1;  iter < maxiter && abs(z) <= LARGE;  iter++) z = z*z + c;
  return iter == maxiter;
}

/*********** Server Code **********************/

void plotResults(int chunkSize, MPI_Status status, int *recvBuffer) {
  int length;
  MPI_Get_count(&status, MPI_INT, &length);

  for (int i = 0;  i < length;  i++) {
    int x = recvBuffer[i] % resolution;
    int y = recvBuffer[i] / resolution;
    if (withgraph) bitmapSetPixel(x, resolution - y - 1);
    /* remember: (0,0) of bitmap corresponds to complex (z0 + i*n) */
  }
  if (withgraph) {
    printf("plotting %d pixels out of %d from PE %i\n", length, chunkSize, status.MPI_SOURCE);
    bitmapDisplay();
  }
}

void dispatchWork(int worker, int start, int chunkSize, int totalSize) {
  int end = start + chunkSize - 1;
  int work[2];
  work[START] = start;
  work[END] = (end < totalSize) ? end : totalSize-1;
  MPI_Send(work, 2, MPI_INT, worker, WORK, MPI_COMM_WORLD);
}

void kill(int worker) {
  int dummy;
  MPI_Send(&dummy, 0, MPI_INT, worker, DONE, MPI_COMM_WORLD);
}

void expectResult(int worker, int chunkSize, int *recvBuffer, MPI_Request *request) {
  MPI_Irecv(recvBuffer, chunkSize, MPI_INT, worker, RESULT, MPI_COMM_WORLD, request);
}

/* From each worker, get a sequence of integers, each
 * encoding one element of the Mandelbrot set. If there
 * is more work left, dispatch it to the reporting working.
 */
void server(char *display, int k) {

  if (withgraph) bitmapInit(resolution, resolution, display, "Mandelbrot");

  int totalSize = resolution * resolution;
  int chunkSize = totalSize / k;

  int totalWorkers = nProc-1;
  int done = 0;

  MPI_Status statuses[totalWorkers];
  MPI_Request requests[totalWorkers];
  int recvBuffers[totalWorkers][chunkSize];
  int indices[totalWorkers];
  int outcount; 

  // Initial work distribution
  for (int i=0; i < totalWorkers; i++) {
    dispatchWork(i+1, done, chunkSize, totalSize); // +1 to map worker to PE-Index
    expectResult(i+1, chunkSize, recvBuffers[i], &requests[i]);
    done += chunkSize; 
  }
  int activeWorkers = totalWorkers;

  // dispatch until done
  while (activeWorkers > 0) {
    MPI_Waitsome(totalWorkers, requests, &outcount, indices, statuses);

    for (int i=0; i < outcount; i++) {
      int worker = statuses[i].MPI_SOURCE;
      if (withgraph) printf("managing worker %i on PE %i \n", indices[i], worker);

      plotResults(chunkSize, statuses[i], recvBuffers[indices[i]]);
      if (done < totalSize) {
        dispatchWork(worker, done, chunkSize, totalSize);
        expectResult(worker, chunkSize, recvBuffers[indices[i]], &requests[indices[i]]);
        done += chunkSize;
      } else {
        if (withgraph) printf("Master is terminating PE %i\n",worker);
        kill(worker);
        activeWorkers--;
      }      
    }
  }
}

/*********** Worker Code **********************/

/* work on an interval of integers representing candidate elements
 * of the Mandelbrot set.
 */
void worker() {
  MPI_Status status;
  int work[2];

  while (1) {
    MPI_Recv(work, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if (status.MPI_TAG == DONE) {
      if (withgraph) printf("PE %i received termination signal!\n", iProc);
      break;
    } else if (withgraph) {
      printf("PE %i starting work in range from %d to %d\n", iProc, work[START], work[END]);
    }
    int result[work[END] - work[START] + 1]; 
    int elem = 0;
    for (int i = work[START]; i <= work[END];  i++) {
      if (isInMandebrotSet(i)) {
        result[elem] = i;
        elem++;
      }
    }
    MPI_Send(result, elem, MPI_INT, 0, RESULT, MPI_COMM_WORLD); 
  }
}

/*********************************************************************/

int main(int argc, char **argv) {
  
  MPI_Init(&argc, &argv);
  assert(argc >= 8);
  assert(argc <= 9);

  float z0Real   = atof(argv[1]);
  float z0Img    = atof(argv[2]);
  z0 = z0Real + I * z0Img ;
  extent         = atof(argv[3]); // a_0
  resolution     = atoi(argv[4]); // n
  step = extent / resolution; 
  maxiter        = atoi(argv[5]); // m_max
  int iterations = atoi(argv[6]);
  int k          = atoi(argv[7]);
  withgraph = (argc == 8) ? 0 : 1;
  char *display  = argv[8];

  if (resolution % (nProc-1) != 0 ) {
    fprintf(stderr,"Work can't be distirbuted equally among %i worker PEs\n", nProc-1);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  assert(nProc >= 2);

  double timings[iterations];

  for (int i = 0;  i < iterations;  i++) {
    MPI_Barrier(MPI_COMM_WORLD); /* synchronize for accurate timing */

    if (iProc == 0) {
      double startTime = MPI_Wtime();
      server(display, k);
      timings[i]= MPI_Wtime() - startTime;
    } else {
      worker();
    }
  }

  if (iProc == 0) {
      if (withgraph) getchar();
      double prunedAvg = pruned_average(timings, iterations, 0.25);
      printf("%i %f %f %g %i %i %i %g # PEs, z0_real, z0_img, a0, n, m_max, iterations time[us] for one iteration \n",
        nProc, z0Real, z0Img, extent, resolution, maxiter, iterations, 1e6 * prunedAvg);
   }

  MPI_Finalize();
  return 0;
}
