A trivial MPI programm without any communication.

 * `hello.c`: Each PE spits out a Hello World.
 * `hello-2.c`: Each PE prints his process id.
