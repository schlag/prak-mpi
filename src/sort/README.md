## Chapter 6 - Sorting

This chapter is concerned with the implementation of various parallel sorting algoritms.

_General:_

* `sequential.c`: Our sequential sorting program (that uses C's standard intsort). It is used as baseline for the other algorithms implemented in this chapter.
* `sort-util.c`: Some utility functions provided by our instructor. These are used in several algorithms in this chapter. 
 
_Odd-even-Transposition-Sort:_

* `oddeven.c`: An implementation of this algorithm provided by our instructor.
 
_Merge-splitting-sort:_

* `mergesplit.c`: An implementation of this algorithm provided by our instructor.

_Sample-Sort:_

We are in need of a `c` so that the sample `c*log(p)` is sufficiently large for proper load balancing.
No PE should receive more than `1.1 * n` elements.

    xcinter 4 samplesort $((2**18)) 50 10
    262144 4 50 10 54994.7 1.09822 # n, p, c, iterations, time[us] for one iteration, maxItemsPerPE/n

We follow the advice in the book and thus conclude that c=50 is sufficient for all our measurements.

* `samplesort.c`: Our implementation of parallel samplesort.
* `my_alltoallv.c`: Provided by our instructor as a hint on how to distribute the buckets.

_Quicksort:_
 
* `pqsimple.c`: An implementation of parallel quicksort with n = 1 elements per PE that was provided by the instructor.
* `quicksort.c`: Our implementation of parallel quicksort with n >> 1 elements per PE.