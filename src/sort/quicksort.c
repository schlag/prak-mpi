#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>
#include "mpi.h" 
#include "sort.h"
#include "random.h"
#include "timing.h"

#define DEBUG_ENABLED 0  
#define Debug(A) //A      /*uncomment for timing */

#define LESSEQ 0
#define GREATER 1

/* random-number generator used for values
 * which are identical on every PE
 * The constants can be found Press Et Al.
 * Numerical Recipes in C, 2nd edition, page 284 
 */
unsigned long globalRandState;
#define sGlobalRand(s) (globalRandState = (s))
#define globalRand() (globalRandState = ((1664525L*globalRandState + 1013904223L) & 0xffffffffL))
#define globalRandInt(n) ((globalRand() >> 10) % (n))


static void printItems(int *items, int n) {
   for (int i = 0; i < n;  i++) {
      printf("%d ", items[i]);
   }
   printf("\n");
}

static int getMPIBuffSize(int n) {
   return n*sizeof(int) + 4 * MPI_BSEND_OVERHEAD;
}

/* povide a big enough buffer for MPI to be able to dispatch all
   data using 4 simultaneous BSENDs */
static void reallocMPIBuffer(int inSize) {
   int size;
   char *ptr;
   MPI_Buffer_detach(&ptr, &size);

   int newSize = inSize * sizeof(int) + 4 * MPI_BSEND_OVERHEAD;
   if (newSize > size) {
      ptr = realloc(ptr, newSize);
   }
   MPI_Buffer_attach(ptr, newSize);
}

static void freeMPIBuffer() {
   int size;
   void *ptr;
   MPI_Buffer_detach(&ptr, &size);
   free(ptr);
}

static int getMedianOfThree(int *items, int n) {
   int c1 = items[(int)(nextRandomLEcuyer() * n)];
   int c2 = items[(int)(nextRandomLEcuyer() * n)];
   int c3 = items[(int)(nextRandomLEcuyer() * n)];
   int large, small;
   if (c1 > c2) {
      large = c1;
      small = c2;
   } else {
      large = c2;
      small = c1;
   }
   if (c3 > large) {
      return large;
   } else if (c3 < small) {
      return small;
   } else {
      return c3;
   }
}

/* determine a pivot */
static int getPivot(int *items, int n, MPI_Comm comm, int numPEs) {
   int pivot = getMedianOfThree(items, n);
   int pivotPE = globalRandInt(numPEs);   /* from random PE */

   /* overwrite pivot by that one from pivotPE */
   MPI_Bcast(&pivot, 1, MPI_INT, pivotPE, comm);
   return pivot;
}

/* determine prefix-sum and overall sum over value */
static void countGlobal(int *value, int *sum, int *prefixSumTotal, MPI_Comm comm, int numPEsInPartition) {  
   MPI_Scan(value, sum, 2, MPI_INT, MPI_SUM, comm);
   prefixSumTotal[0] = sum[0];
   prefixSumTotal[1] = sum[1];
   MPI_Bcast(prefixSumTotal, 2, MPI_INT, numPEsInPartition - 1, comm);
   sum[LESSEQ] -= value[LESSEQ];
   sum[GREATER] -= value[GREATER];
}

static int calculatePartitionSize(int numPEsInPartition, int *prefixSumTotal) {
   double initialS = (double)(prefixSumTotal[LESSEQ]*numPEsInPartition) /
      (prefixSumTotal[LESSEQ]+prefixSumTotal[GREATER]);

   Debug(printf("s = %g, g = %g \n",initialS));

   if (initialS < 1) {
      return ceil(initialS);
   } else if (numPEsInPartition-initialS < 1) {
      return floor(initialS);
   } else if (initialS - floor(initialS) > 0.5) {
      return ceil(initialS);
   } else {
      return floor(initialS);
   }
}

static void distributeItems(int *items, int startPE, int fillLevel, int remaining, int bufferSize, MPI_Comm comm) {
   int sendCount;
   int totalSendCount = 0;

   while (remaining > 0) {
      sendCount = bufferSize - fillLevel;
      if (sendCount > remaining) {
         sendCount = remaining;
      }
      Debug(printf("PE %i: sending %i items to PE %i\n", partitionRank, sendCount, startPE);)
      MPI_Bsend(items+totalSendCount, sendCount, MPI_INT, startPE, 88, comm);
      startPE++;
      fillLevel = 0;
      totalSendCount += sendCount;
      remaining -= sendCount;
   }  
}

static void quickSort(int **items, int inSize, int *outSize, MPI_Comm comm) {
   int numPEsInPartition, partitionRank;
   int designatedPartition;
   int designatedPartitionSize;
   int desigantedBufferSize;
   int isLastPEInDesigantedPartition;
   MPI_Status status;

   MPI_Comm_size(comm, &numPEsInPartition);
   MPI_Comm_rank(comm, &partitionRank);

   while (numPEsInPartition > 1) {
      reallocMPIBuffer(inSize);

      int pivot = getPivot(*items, inSize, comm, numPEsInPartition);
      int splitIndex = split(*items, inSize, pivot);

      int localSum[2] = {splitIndex, inSize-splitIndex};
      int prefixSum[2] = {0, 0}; 
      int prefixSumTotal[2] = {0, 0};
      countGlobal(localSum, prefixSum, prefixSumTotal, comm, numPEsInPartition);
            
      int sPartitionSize = calculatePartitionSize(numPEsInPartition, prefixSumTotal);
      int gPartitionSize = numPEsInPartition - sPartitionSize;

      /* calculate size of buffers for received small / received large items */
      int sBufferSize = ceil(prefixSumTotal[LESSEQ]/(double)sPartitionSize);
      int gBufferSize = ceil(prefixSumTotal[GREATER]/(double)gPartitionSize);

      if (DEBUG_ENABLED && partitionRank == 0) {
         printf("sPartitionSize: %i, gPartitionSize %i\n",sPartitionSize,gPartitionSize );
         printf("sBufferSize: %i, gBufferSize: %i\n",sBufferSize,gBufferSize);
      }

      // Send elements in the left partition.
      int startPE = prefixSum[LESSEQ] / sBufferSize;
      int fillLevel = prefixSum[LESSEQ] % sBufferSize;
      int numOfElementsToSend = localSum[LESSEQ];
      distributeItems(*items, startPE, fillLevel, numOfElementsToSend, sBufferSize, comm);
 
      // Send elements in the right partition.
      startPE = sPartitionSize + prefixSum[GREATER] / gBufferSize;
      fillLevel = prefixSum[GREATER] % gBufferSize;
      numOfElementsToSend = localSum[GREATER];
      distributeItems((*items)+splitIndex, startPE, fillLevel, numOfElementsToSend, gBufferSize, comm);

      // Prepare for receive
      if (partitionRank < sPartitionSize) {
         designatedPartitionSize = sPartitionSize;
         desigantedBufferSize = sBufferSize;
         isLastPEInDesigantedPartition = partitionRank == sPartitionSize-1;
	 designatedPartition = LESSEQ;
      } else {
         designatedPartitionSize = gPartitionSize;
         desigantedBufferSize = gBufferSize;
         isLastPEInDesigantedPartition = partitionRank == numPEsInPartition-1;
	 designatedPartition = GREATER;
      }

      if (desigantedBufferSize > inSize) { // Only enlarge, don't shrink. 
         *items = (int *) realloc(*items, desigantedBufferSize*sizeof(int));
      }

      int expectedItems = isLastPEInDesigantedPartition ? prefixSumTotal[designatedPartition]-
	      desigantedBufferSize*(designatedPartitionSize-1) : desigantedBufferSize;
      int numOfReceivedItems = 0;
      int totalNumOfReceivedItems = 0; 

      // Receive all incoming elements
      Debug(printf("PE %i expecting %i items\n", partitionRank, expectedItems);)
      while (totalNumOfReceivedItems < expectedItems ) {
         MPI_Recv((*items)+totalNumOfReceivedItems, desigantedBufferSize, MPI_INT, MPI_ANY_SOURCE, 88, comm, &status);
         MPI_Get_count(&status, MPI_INT, &numOfReceivedItems);
         totalNumOfReceivedItems = totalNumOfReceivedItems + numOfReceivedItems;
         Debug(printf("PE %i received %i items from PE %i\n", partitionRank, numOfReceivedItems, status.MPI_SOURCE);)
      }
      if (DEBUG_ENABLED) {
         printf("PE %i received %i items: \n",partitionRank,totalNumOfReceivedItems);
         printItems(*items,totalNumOfReceivedItems);
      }

      MPI_Comm_split(comm, partitionRank < sPartitionSize , 0, &comm);
      MPI_Comm_size(comm, &numPEsInPartition);
      MPI_Comm_rank(comm, &partitionRank);
      inSize = totalNumOfReceivedItems;
   }

   intSort(*items, inSize);
   *outSize = inSize;
}

int main(int argc, char** argv) {  
   MPI_Init(&argc, &argv);

   if (argc != 3) {
      fprintf(stderr,"\n Synopsis: quicksort <ElementsPerPE> <#Repetitions>!\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
   }

   int n = atoi(argv[1]);
   int its  = atoi(argv[2]);

   int numPEs, rank;
   MPI_Comm_size(MPI_COMM_WORLD, &numPEs);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   MPI_Buffer_attach(malloc(getMPIBuffSize(n)), getMPIBuffSize(n));

   /* initialize random-number generators */
   initParallelRandomLEcuyer(424243, rank, numPEs);

   double timings[its];
   double fractions[its];

   MPI_Barrier(MPI_COMM_WORLD);     /* synchronize for accurate timing */

   for (int i = 0;  i < its;  i++) {
      int *items = malloc(n*sizeof(int));
      randInit(items, n);

      if (DEBUG_ENABLED) {
         printItemsGlobally(items, n);
      }

      sGlobalRand(42424 + (1 << 17)); // reset; all PEs have to produce the same pivots
      double startTime = MPI_Wtime();
      
      int outSize;   
      quickSort(&items, n, &outSize, MPI_COMM_WORLD);

      double localTime = MPI_Wtime() - startTime;
      MPI_Reduce(&localTime, &timings[i], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      double fraction = ((double) outSize) / n;
      MPI_Reduce(&fraction, &fractions[i], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      if (DEBUG_ENABLED) {
         printItemsGlobally(items, outSize);
         printf("i=%i PE %i has %i elements: %i - %i. Is sorted: %i\n", i, rank, outSize, items[0], items[outSize-1], isGloballySorted(items, outSize));
      }
      assert(isGloballySorted(items, outSize));
      free(items);
   }

   if (rank == 0) {
      double prunedAvg = pruned_average(timings, its, 0.25);
      double prunedFractionAvg = pruned_average(fractions, its, 0.25);
      printf("%i %i %i %g %g # n, p, iterations, time[us] for one iteration, maxItemsPerPE/n  \n",
         n, numPEs, its, 1e6 * prunedAvg, prunedFractionAvg);
   }

   freeMPIBuffer();
   MPI_Finalize();
   return 0;
}
