#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>
#include "mpi.h" 
#include "sort.h"
#include "random.h"
#include "timing.h"

#define DEBUG_ENABLED 0 

void printItems(int *items, int n) {
   for (int i = 0; i < n;  i++) {
      printf("%d ", items[i]);
   }
   printf("\n");
}

static void computeSplitters(int *splitters, int numSplitters, int *items, int n, int c,
      int numPEs, int rank, MPI_Comm comm) {  
   int m = c * log2(numPEs);
   m = m >= numSplitters-2 ? m : numSplitters-2;
   int sample[m];

   /* Fetch a random sample */
   for (int i = 0; i < m; i++) {
      sample[i] = items[(int)(nextRandomLEcuyer() * n)];
   }

   int globalsample[m * numPEs];
   MPI_Allgather(sample, m, MPI_INTEGER, globalsample, m, MPI_INTEGER, comm);

   if (DEBUG_ENABLED && rank == 0) {
      printf("Sample: ");
      printItems(globalsample, m*numPEs);
   }

   intSort(globalsample, m * numPEs);

   /* Extract splitters from sample, +/- infinity at the beginning and end  */
   splitters[0] = -INT_MAX;
   for (int i = 1; i < numSplitters-1; i++) {
      splitters[i] = globalsample[i * m];
   }
   splitters[numSplitters-1] = INT_MAX;

   if (DEBUG_ENABLED && rank == 0) {
      printf("Splitters: ");
      printItems(splitters, numSplitters);
   }
}

static void printBuckets(int **buckets, int numPEs, int rank) {
   if (DEBUG_ENABLED) {
      sleep(rank);
      printf("PE %d: \n",rank);
      for (int i=0; i<numPEs; i++) {
         printf("  Bucket %d\t", i);
         printItems(buckets[i], buckets[i+1]-buckets[i]);
      }
   }
}

static int sum(int *array, int n) {
   int sum = 0;
   for (int i=0; i<n; i++) {
      sum += array[i];
   }
   return sum;
}

/* Compute the (exclusive) prefix sum over array and store in result */
static void scan(int *array, int n, int *result) {
   int sum = 0;
   for (int i=0; i<n; i++) {
      result[i] = sum;
      sum += array[i];
   }
}

static void distributeBuckets(int **buckets, int numPEs, int rank, int **receivedItems,
      int *numOfReceivedItems, MPI_Comm comm) {
   /* Find out how many elements each PE will receive from us */
   int bucketSize[numPEs];
   for (int i=0; i<numPEs; i++) {
      bucketSize[i] = buckets[i+1] - buckets[i];
   }
   /* Find out how many elements we will receive from other PEs. */
   int numOfItemsPerPE[numPEs];
   MPI_Alltoall(bucketSize, 1, MPI_INTEGER, numOfItemsPerPE, 1, MPI_INTEGER, comm);
   
   *numOfReceivedItems = sum(numOfItemsPerPE, numPEs);
   *receivedItems = malloc(*numOfReceivedItems * sizeof(int));

   /* We have no fancy offsets/displacements. Simple prefix sum suffices */   
   int sdispls[numPEs];
   scan(bucketSize, numPEs, sdispls);
   int rdispls[numPEs];
   scan(numOfItemsPerPE, numPEs, rdispls);

   MPI_Alltoallv(buckets[0], bucketSize, sdispls, MPI_INTEGER, *receivedItems,
      numOfItemsPerPE, rdispls, MPI_INTEGER, comm);
}

static void sampleSort(int *items, int n, int c, int numPEs, int rank, MPI_Comm comm,
      int **receivedItems, int *numOfReceivedItems) {  

   int numSplitters = numPEs-1 + 2; /* +2 for +/- infinity at the beginning and end */
   int splitters[numSplitters];
   computeSplitters(splitters, numSplitters, items, n, c, numPEs, rank, comm);

   /* A bucket is represented by a pointer into the partially sorted items array */
   int *buckets[numSplitters];
   partition(items, n, splitters, buckets, numSplitters);
   printBuckets(buckets, numPEs, rank);

   distributeBuckets(buckets, numPEs, rank, receivedItems, numOfReceivedItems, comm);
   
   intSort(*receivedItems, *numOfReceivedItems);
} 

int main(int argc, char** argv) {  
   MPI_Init(&argc, &argv);

   if(argc != 4){
      fprintf(stderr,"\n Synopsis: samplesort <#ElementsPerPE> <c> <#Repetitions>!\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
   }
   int n = atoi(argv[1]); /* num of elements per PE to sort */
   int c = atoi(argv[2]); /* sample size tuning parameter */
   int its = atoi(argv[3]); /* num of repititions */

   int numPEs, rank;
   MPI_Comm_size(MPI_COMM_WORLD, &numPEs);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   double timings[its];
   double fractions[its];
   int *items = malloc(n*sizeof(int));

   initParallelRandomLEcuyer(424243, rank, numPEs);
   MPI_Barrier(MPI_COMM_WORLD);     /* synchronize for accurate timing */

   for (int i = 0; i < its; i++) {
      randInit(items, n);

      if (DEBUG_ENABLED) {
         printItemsGlobally(items, n);
      }
      double startTime = MPI_Wtime();

      int *receivedItems;
      int numOfReceivedItems;
      sampleSort(items, n, c, numPEs, rank, MPI_COMM_WORLD, &receivedItems, &numOfReceivedItems);

      double localTime = MPI_Wtime() - startTime;
      MPI_Reduce(&localTime, &timings[i], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      double fraction = ((double) numOfReceivedItems) / n;
      MPI_Reduce(&fraction, &fractions[i], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      if (DEBUG_ENABLED) {
         printItemsGlobally(receivedItems, numOfReceivedItems);
      }
      assert(isGloballySorted(receivedItems, numOfReceivedItems));
      free(receivedItems);
   }

   if (rank == 0) {
      double prunedAvg = pruned_average(timings, its, 0.25);
      double prunedFractionAvg = pruned_average(fractions, its, 0.25);
      printf("%i %i %i %i %g %g # n, p, c, iterations, time[us] for one iteration, maxItemsPerPE/n \n",
         n, numPEs, c, its, 1e6 * prunedAvg, prunedFractionAvg);
   }
    
   free(items);
   MPI_Finalize();
   return 0;
}
