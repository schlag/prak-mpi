#include <stdlib.h>
#include <stdio.h>
#include "sort.h"
#include "timing.h"
#include <mpi.h>
#include <math.h>


int main(int argc, char* argv[]) {
	MPI_Init(&argc, &argv);

	int n = atoi(argv[1]);
	int iters = atoi(argv[2]);

	int *keys = malloc(n*sizeof(int));	
	double *timings = malloc(iters*sizeof(double));
	
	for (int i=0; i<iters; i++) {
		randInit(keys, n);
		double time = MPI_Wtime();
		intSort(keys, n);
		timings[i] = MPI_Wtime() - time;
	}
	double avg = pruned_average(timings, iters, 0.25) *1e6;
	double optimum = n*log(n);
	printf("%d %g %g %g # n, runtime[µs], nlogn, time for a cmpr c=runtime/(comparisons nlogn) \n", n, avg, optimum, avg/optimum); 
		
	MPI_Finalize();
	return 0;
}
