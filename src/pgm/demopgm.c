
#include "pgm.h"
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARGC 3

int main ( int argc , char ** argv )
{
  char * filename ;
  int * data ;
  int size ;
  int m_max ;
  int filename_len ;
  int i ;
  int numProcs ;
  int myID ;

  MPI_Init ( & argc , & argv ) ;

  MPI_Comm_size ( MPI_COMM_WORLD , & numProcs ) ;
  MPI_Comm_rank ( MPI_COMM_WORLD , & myID ) ;

  if ( argc != ARGC + 1 )
    {
      fprintf ( stderr , "usage: %s size m_max filename\n") ; 
      MPI_Abort ( MPI_COMM_WORLD , 1 ) ; exit ( 1 ) ;
    }

  size = abs ( atoi ( argv [ 1 ] ) ) ;
  m_max = abs ( atoi ( argv [ 2 ] ) ) ;

  filename_len = strlen ( argv [ 3 ] ) ;

  filename = (char *) malloc ( ( filename_len + 1 ) * sizeof (char)) ;
  
  strncpy ( filename , argv [ 3 ] , filename_len + 1 ) ;

  fprintf ( stdout 
	  , "filename = %s, size = %d, m_max = %d\n"
	  , filename , size , m_max 
          ) ;

  data = (int *) malloc ( size * size * sizeof ( int ) ) ;

  for ( i = 0 ; i < size * size ; i ++ )
    {
      data [ i ] = ( i / size ) % m_max ;
    }

  if ( myID == 0 )
    {
      pgm ( filename , data , size , m_max ) ;
    }

  free ( data ) ;
  free ( filename ) ;

  MPI_Finalize () ;

  return 0 ;
}
