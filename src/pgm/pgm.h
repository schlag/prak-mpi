
#ifndef H_PGM
#define H_PGM

/* 
 * bild immer quadratisch
 * daten als lineares feld von int
 * werte im feld aus [0..m_max]
 */

void pgm ( char * filename , int * data , int size , int m_max ) ;

#endif
