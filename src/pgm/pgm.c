
#include "pgm.h"
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void pgm ( char * filename , int * data , int size , int m_max )
{
  FILE * out = fopen ( filename , "w" ) ;
  int i = 0 ;

  if ( out == NULL )
    {
      fprintf ( stderr , "error opening output file %s\n" , filename ) ;

      MPI_Abort ( MPI_COMM_WORLD , 1 ) ; 

      exit ( 1 ) ;
    }

  fprintf ( out , "P2\n%d %d\n%d\n" , size , size , m_max ) ;

  for ( i = 0 ; i < size * size ; i ++ )
    {
      fprintf ( out , "%d " , data [ i ] ) ;
    }

  fclose ( out ) ;

  return ;
}
