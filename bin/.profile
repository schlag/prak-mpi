set -o emacs
export PATH=$HOME/praktikum/bin:$PATH
# export MP_RMPOOL=0
# export MP_PROCS=1
# export MP_EUIDEVICE=css0
# export MP_CPU_USE=multiple
# export MP_EUILIB=ip
# export RZ_GDB=GDB_416
export RZ_EMACS=EMACS_207
. .bashrc
