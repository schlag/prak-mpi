export PS1='\[\e[1m\]\u@\h:\w \#>\[\e[m\]'
alias ll='ls -lF'
export MP_RMPOOL=0
export MP_PROCS=1
export MP_NODES=1
export MP_EUIDEVICE=css0
export MP_CPU_USE=multiple
export MP_EUILIB=ip
export MP_LABELIO=yes
export MP_STDOUTMODE=unordered
export MP_EUIDEVELOP=no
export RZ_GDB=GDB_50
export RZ_EMACS=EMACS_207
